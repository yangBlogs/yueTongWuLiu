package com.eihei.yueTongWuLiu.controller;

import com.eihei.yueTongWuLiu.pojo.ChengYunDan;
import com.eihei.yueTongWuLiu.service.impl.ChengYunDanServiceImpl;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 承运单表(ChengYunDan)表控制层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@RestController
@RequestMapping("chengYunDan")
@Api(value = "承运单表(ChengYunDan)管理",tags = "承运单表(ChengYunDan)管理接口API")
@ApiResponses({
        @ApiResponse(code=400,message="请求参数不完整或者错误"),
        @ApiResponse(code=404,message="找不到页面"),
        @ApiResponse(code=405,message="请求方式不正确，比如后台接收是post，但前端请求的是get"),
        @ApiResponse(code=406,message="页面地址和接口名冲突"),
        @ApiResponse(code=415,message="请求格式不正确，前端可能没有把请求头修改为json，也可能前端请求的json对象没有转换为字符串"),
        @ApiResponse(code=500,message="后台服务逻辑错误")
})
public class ChengYunDanController {
    /**
     * 服务对象
     */
    @Resource
    private ChengYunDanServiceImpl chengYunDanServiceImpl;

    /**
     * 通过主键查询单条数据
     * @param page  需要查询的页码
     * @return 单条数据
     */
    @RequestMapping(value = "chaXunFenYe", method = RequestMethod.GET)
    @ApiOperation(value = "列表查询分页接口",notes = "列表查询分页接口",httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "页码",paramType = "query",dataType = "String",required = true),
            @ApiImplicitParam(name = "faHuoRen",value = "发货人",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "shouHuoRen",value = "收货人",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "wanChengQingKuang",value = "完成情况",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "id",value = "Id",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "userId",value = "用户Id",paramType = "query",dataType = "String",required = false)
    })
    public Map<String, Object> chaXunFenYe(@RequestParam(name = "page") int page, String faHuoRen,String shouHuoRen,String wanChengQingKuang,String userId,Integer id) {
        return this.chengYunDanServiceImpl.chaXunFenYe(page, faHuoRen,shouHuoRen,wanChengQingKuang,userId,id);
    }
    
    /**
     * 查询所有数据
     * @return 返回所有数据
     */
    @RequestMapping(value = "chaXunAll", method = RequestMethod.GET)
    @ApiOperation(value = "查询所有数据接口",notes = "查询所有数据接口",httpMethod = "GET")
    public Map<String, Object> chaXunAll() {
        return this.chengYunDanServiceImpl.chaXunAll();
    }
    
    /**
     * 通过主键删除单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @RequestMapping(value = "shanChuById", method = RequestMethod.POST)
    @ApiOperation(value = "根据一个主键删除一条承运单记录的接口",notes = "根据一个主键删除一条承运单记录的接口",httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "主键",paramType = "query",dataType = "String",required = true),
            @ApiImplicitParam(name = "usernmae",value = "usernmae",paramType = "query",dataType = "String",required = true)
    })
    public Map<String, Object> shanChuById(@RequestParam("id") Integer id, String usernmae, HttpServletRequest request) {
        return this.chengYunDanServiceImpl.shanChuById(id,usernmae,request);
    }
    
    /**
     * 通过主键更新单条数据
     *
     * @param chengYunDan 一个数据库对应的POJO数据对象
     * @return 单条数据
     */
    @RequestMapping(value = "gengXinById", method = RequestMethod.POST)
    @ApiOperation(value = "通过主键更新一个承运单的接口",notes = "通过主键更新一个承运单的接口",httpMethod = "POST")
    public Map<String, Object> gengXinById(@RequestBody @ApiParam(name = "chengYunDan",value = "pojo模型",required = true) ChengYunDan chengYunDan) {
        return this.chengYunDanServiceImpl.gengXinById(chengYunDan);
    }
    
    /**
     * 通过一个pojo对象新增单条数据
     *
     * @param chengYunDan 一个数据库对应的POJO数据对象
     * @return 返回插入的主键id
     */
    @RequestMapping(value = "xinZeng", method = RequestMethod.POST)
    @ApiOperation(value = "根据完整记录新增一个承运单的接口",notes = "根据完整记录新增一个承运单的接口",httpMethod = "POST")
    public Map<String, Object> xinZeng(@RequestBody @ApiParam(name = "chengYunDan",value = "pojo模型",required = true) ChengYunDan chengYunDan) {
        return this.chengYunDanServiceImpl.xinZeng(chengYunDan);
    }

}