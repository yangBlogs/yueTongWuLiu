package com.eihei.yueTongWuLiu.controller;

import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import com.eihei.yueTongWuLiu.service.impl.XiTongRiZhiServiceImpl;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 系统日志表(XiTongRiZhi)表控制层
 *
 * @author yangToT
 * @since 2021-03-25 09:52:10
 * @version 1.0
 */
@RestController
@RequestMapping("xiTongRiZhi")
@Api(value = "系统日志表(XiTongRiZhi)管理",tags = "系统日志表(XiTongRiZhi)管理接口API")
@ApiResponses({
        @ApiResponse(code=400,message="请求参数不完整或者错误"),
        @ApiResponse(code=404,message="找不到页面"),
        @ApiResponse(code=405,message="请求方式不正确，比如后台接收是post，但前端请求的是get"),
        @ApiResponse(code=406,message="页面地址和接口名冲突"),
        @ApiResponse(code=415,message="请求格式不正确，前端可能没有把请求头修改为json，也可能前端请求的json对象没有转换为字符串"),
        @ApiResponse(code=500,message="后台服务逻辑错误")
})
public class XiTongRiZhiController {
    /**
     * 服务对象
     */
    @Resource
    private XiTongRiZhiServiceImpl xiTongRiZhiServiceImpl;

    /**
     * 通过主键查询单条数据
     * @param page  需要查询的页码
     * @return 单条数据
     */
    @RequestMapping(value = "chaXunFenYe", method = RequestMethod.GET)
    @ApiOperation(value = "列表查询分页接口",notes = "列表查询分页接口",httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "页码",paramType = "query",dataType = "String",required = true),
            @ApiImplicitParam(name = "caoZuoXingWei",value = "操作行为",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "username",value = "username",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "xieRuShiJian_KaiSHi",value = "写入时间 开始时间",paramType = "query",dataType = "String",required = false),
            @ApiImplicitParam(name = "xieRuShiJian_JieShu",value = "写入时间 结束时间",paramType = "query",dataType = "String",required = false)
    })
    public Map<String, Object> chaXunFenYe(@RequestParam(name = "page") int page, String caoZuoXingWei,String username,String xieRuShiJian_KaiSHi,String xieRuShiJian_JieShu ) {
        return this.xiTongRiZhiServiceImpl.chaXunFenYe(page, caoZuoXingWei,username,xieRuShiJian_KaiSHi,xieRuShiJian_JieShu);
    }
    
    /**
     * 通过主键删除单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @RequestMapping(value = "shanChuById", method = RequestMethod.POST)
    @ApiOperation(value = "根据一个主键删除一条**记录的接口",notes = "根据一个主键删除一条**记录的接口",httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "主键",paramType = "query",dataType = "String",required = true),
    })
    public Map<String, Object> shanChuById(@RequestParam("id") String id) {
        return this.xiTongRiZhiServiceImpl.shanChuById(id);
    }


}