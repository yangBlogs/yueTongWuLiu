package com.eihei.yueTongWuLiu.controller;

import com.eihei.yueTongWuLiu.pojo.JueSe;
import com.eihei.yueTongWuLiu.service.impl.JueSeServiceImpl;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 角色表(JueSe)表控制层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@RestController
@RequestMapping("jueSe")
@Api(value = "角色表(JueSe)管理",tags = "角色表(JueSe)管理接口API")
@ApiResponses({
        @ApiResponse(code=400,message="请求参数不完整或者错误"),
        @ApiResponse(code=404,message="找不到页面"),
        @ApiResponse(code=405,message="请求方式不正确，比如后台接收是post，但前端请求的是get"),
        @ApiResponse(code=406,message="页面地址和接口名冲突"),
        @ApiResponse(code=415,message="请求格式不正确，前端可能没有把请求头修改为json，也可能前端请求的json对象没有转换为字符串"),
        @ApiResponse(code=500,message="后台服务逻辑错误")
})
public class JueSeController {
    /**
     * 服务对象
     */
    @Resource
    private JueSeServiceImpl jueSeServiceImpl;

    /**
     * 查询所有数据
     * @return 返回所有数据
     */
    @RequestMapping(value = "chaXunAll", method = RequestMethod.GET)
    @ApiOperation(value = "查询所有数据接口",notes = "查询所有数据接口",httpMethod = "GET")
    public Map<String, Object> chaXunAll() {
        return this.jueSeServiceImpl.chaXunAll();
    }
}