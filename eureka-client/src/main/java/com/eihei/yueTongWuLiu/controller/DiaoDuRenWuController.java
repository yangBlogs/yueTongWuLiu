package com.eihei.yueTongWuLiu.controller;

import com.eihei.yueTongWuLiu.pojo.DiaoDuRenWu;
import com.eihei.yueTongWuLiu.service.impl.DiaoDuRenWuServiceImpl;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 调度任务信息表(DiaoDuRenWu)表控制层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@RestController
@RequestMapping("diaoDuRenWu")
@Api(value = "调度任务信息表(DiaoDuRenWu)管理",tags = "调度任务信息表(DiaoDuRenWu)管理接口API")
@ApiResponses({
        @ApiResponse(code=400,message="请求参数不完整或者错误"),
        @ApiResponse(code=404,message="找不到页面"),
        @ApiResponse(code=405,message="请求方式不正确，比如后台接收是post，但前端请求的是get"),
        @ApiResponse(code=406,message="页面地址和接口名冲突"),
        @ApiResponse(code=415,message="请求格式不正确，前端可能没有把请求头修改为json，也可能前端请求的json对象没有转换为字符串"),
        @ApiResponse(code=500,message="后台服务逻辑错误")
})
public class DiaoDuRenWuController {
    /**
     * 服务对象
     */
    @Resource
    private DiaoDuRenWuServiceImpl diaoDuRenWuServiceImpl;

    /**
     * 通过主键查询单条数据
     * @param page  需要查询的页码
     * @return 单条数据
     */
    @RequestMapping(value = "chaXunFenYe", method = RequestMethod.GET)
    @ApiOperation(value = "列表查询分页接口",notes = "列表查询分页接口",httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "页码",paramType = "query",dataType = "String",required = true),
    })
    public Map<String, Object> chaXunFenYe(@RequestParam(name = "page") int page) {
        return this.diaoDuRenWuServiceImpl.chaXunFenYe(page);
    }
    
    /**
     * 查询所有数据
     * @return 返回所有数据
     */
    @RequestMapping(value = "chaXunAll", method = RequestMethod.GET)
    @ApiOperation(value = "查询所有数据接口",notes = "查询所有数据接口",httpMethod = "GET")
    public Map<String, Object> chaXunAll() {
        return this.diaoDuRenWuServiceImpl.chaXunAll();
    }
    
    /**
     * 通过主键删除单条数据
     *
     * @param  diaoDuRenWu 一个数据库对应的POJO数据对象
     * @return 单条数据
     */
    @RequestMapping(value = "shanChuById", method = RequestMethod.POST)
    @ApiOperation(value = "根据一个主键删除一条调度任务记录的接口",notes = "根据一个主键删除一条调度任务记录的接口",httpMethod = "POST")
    public Map<String, Object> shanChuById(@RequestBody @ApiParam(name = "diaoDuRenWu",value = "pojo模型",required = true) DiaoDuRenWu diaoDuRenWu) {
        return this.diaoDuRenWuServiceImpl.shanChuById(diaoDuRenWu);
    }
    
    /**
     * 通过主键更新单条数据
     *
     * @param diaoDuRenWu 一个数据库对应的POJO数据对象
     * @return 单条数据
     */
    @RequestMapping(value = "gengXinById", method = RequestMethod.POST)
    @ApiOperation(value = "通过主键更新一个调度任务的接口",notes = "通过主键更新一个调度任务的接口",httpMethod = "POST")
    public Map<String, Object> gengXinById(@RequestBody @ApiParam(name = "diaoDuRenWu",value = "pojo模型",required = true) DiaoDuRenWu diaoDuRenWu) {
        return this.diaoDuRenWuServiceImpl.gengXinById(diaoDuRenWu);
    }
    
    /**
     * 通过一个pojo对象新增单条数据
     *
     * @param diaoDuRenWu 一个数据库对应的POJO数据对象
     * @return 返回插入的主键id
     */
    @RequestMapping(value = "xinZeng", method = RequestMethod.POST)
    @ApiOperation(value = "根据完整记录新增一个调度任务的接口",notes = "根据完整记录新增一个调度任务的接口",httpMethod = "POST")
    public Map<String, Object> xinZeng(@RequestBody @ApiParam(name = "diaoDuRenWu",value = "pojo模型",required = true) DiaoDuRenWu diaoDuRenWu) {
        return this.diaoDuRenWuServiceImpl.xinZeng(diaoDuRenWu);
    }

}