package com.eihei.yueTongWuLiu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @author yangToT
 * @version 1.0
 * @description EurekaclientApplication.java
 * @date 2021/3/15 20:51
 */
@SpringBootApplication
@EnableEurekaClient
@MapperScan("com.eihei.yueTongWuLiu.mapper")
public class EurekaclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(EurekaclientApplication.class, args);
    }

}
