package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.CheDuiXinXiMapper;
import com.eihei.yueTongWuLiu.mapper.XiTongRiZhiMapper;
import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.pojo.CheLiangXinXi;
import com.eihei.yueTongWuLiu.mapper.CheLiangXinXiMapper;
import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import com.eihei.yueTongWuLiu.util.IPHelper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.CheLiangXinXiService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.sql.PreparedStatement;
import java.util.*;

/**
 * 车辆信息表(CheLiangXinXi)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-17 10:14:07
 */
@Service("cheLiangXinXiService")
public class CheLiangXinXiServiceImpl implements CheLiangXinXiService {
    @Resource
    private CheLiangXinXiMapper cheLiangXinXiMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private XiTongRiZhiMapper xiTongRiZhiMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.cheLiangXinXiMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("*cheLiangXinXi-*");
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(keys);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.cheLiangXinXiMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page, String chePaiHaoMa, String cheLiangLeiXin, Integer dunWei, Integer cheDuiXinXiId, Integer gongZuoZhuangTai) {
        Map<String, Object> map = new HashMap<>();

        String key = "cheLiangXinXi-*";
        int count = 0;
        boolean chePaiHaoMa_action = false;
        boolean cheLiangLeiXin_action = false;
        boolean dunWei_action = false;
        boolean cheDuiXinXiId_action = false;
        boolean gongZuoZhuangTai_action = false;
        if (chePaiHaoMa != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                chePaiHaoMa_action = true;
                key += "chePaiHaoMa:" + "*" + chePaiHaoMa + "*,id*";
                System.out.println(key);
            }
        }
        if (cheLiangLeiXin != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                cheLiangLeiXin_action = true;
                key += "cheLiangLeiXin:" + cheLiangLeiXin + ",*";
                System.out.println(key);
            } else if (count == 2) {
                cheLiangLeiXin_action = true;
                key += "cheLiangLeiXin:" + cheLiangLeiXin + ",*";
                System.out.println(key);
            }
        }
        if (dunWei != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                dunWei_action = true;
                key += "dunWei:" + dunWei + ",*";
                System.out.println(key);
            } else if (count == 2) {
                dunWei_action = true;
                key += "dunWei:" + dunWei + ",*";
                System.out.println(key);
            } else if (count == 3) {
                dunWei_action = true;
                key += "dunWei:" + dunWei + ",*";
                System.out.println(key);
            }
        }
        if (cheDuiXinXiId != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                cheDuiXinXiId_action = true;
                key += "cheDuiXinXiId:" + cheDuiXinXiId + ",*";
                System.out.println(key);
            } else if (count == 2) {
                cheDuiXinXiId_action = true;
                key += "cheDuiXinXiId:" + cheDuiXinXiId + ",*";
                System.out.println(key);
            } else if (count == 3) {
                cheDuiXinXiId_action = true;
                key += "cheDuiXinXiId:" + cheDuiXinXiId + ",*";
                System.out.println(key);
            } else if (count == 4) {
                cheDuiXinXiId_action = true;
                key += "cheDuiXinXiId:" + cheDuiXinXiId + ",*";
                System.out.println(key);
            }
        }
        if (gongZuoZhuangTai != null) {
            count++;
            if (count == 1) {
                gongZuoZhuangTai_action = true;
                key += "gongZuoZhuangTai:" + gongZuoZhuangTai + ",*";
                System.out.println(key);
            } else if (count == 2 || count == 3 || count == 4 || count ==5) {
                gongZuoZhuangTai_action = true;
                key += "gongZuoZhuangTai:" + gongZuoZhuangTai + ",*";
                System.out.println(key);
            }
        }
        System.out.println("----=" + key);
        Set<String> keys = redisTemplate.keys(key);
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param cheLiangXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(CheLiangXinXi cheLiangXinXi) {
        Map<String, Object> map = new HashMap<>();
        if (cheLiangXinXi.getChePaiHaoMa() != null && cheLiangXinXi.getDunWei() != null) {
            try {
                Set<String> cheDuiXinXiKeys = redisTemplate.keys("cheDuiXinXi-" + "*" + "id:" + cheLiangXinXi.getCheDuiXinXiId() + "" + ",*");
                List<CheDuiXinXi> cheDuiXinXiList = redisTemplate.opsForValue().multiGet(cheDuiXinXiKeys);
                for (CheDuiXinXi cheDuiXinXi : cheDuiXinXiList) {
                    cheLiangXinXi.setCheDuiXinXi(cheDuiXinXi);
                    cheLiangXinXi.setChuangJianShiJian(new Date());
                    cheLiangXinXi.setGongZuoZhuangTai(0);
                    cheLiangXinXi.setCheLiangShiFouBangDing(0);
                    this.cheLiangXinXiMapper.xinZeng(cheLiangXinXi);
                    int cheLiangXinXiId = cheLiangXinXiMapper.chaXunId(cheLiangXinXi.getChePaiHaoMa());
                    cheLiangXinXi.setId(cheLiangXinXiId);
                    if (cheLiangXinXiId != 0) {
                        redisTemplate.opsForValue().set("cheLiangXinXi-" + "chePaiHaoMa:" + cheLiangXinXi.getChePaiHaoMa() + "," +
                                "id:" + cheLiangXinXiId + "," + "cheLiangLeiXin:" + cheLiangXinXi.getCheLiangLeiXin() + "," +
                                "dunWei:" + cheLiangXinXi.getDunWei() + "," + "cheDuiXinXiId:" + cheLiangXinXi.getCheDuiXinXiId() + "," +
                                "gongZuoZhuangTai:" + cheLiangXinXi.getGongZuoZhuangTai() + "," + "cheLiangShiFouBangDing:" + cheLiangXinXi.getCheLiangShiFouBangDing(), cheLiangXinXi);
                        map.put("code", 200);
                        map.put("msg", "新增成功");
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 100200);
                map.put("msg", "新增失败，请稍候重试！");
            }
        } else {
            map.put("code", 10000);
            map.put("msg", "新增失败,请填写必填项");
        }
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param cheLiangXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(CheLiangXinXi cheLiangXinXi) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("cheLiangXinXi-" + "*" + "id:" + cheLiangXinXi.getId() + ",*");
        if (keys.size() != 0) {
            for (String str : keys) {
                try {
                    //删除Redis中此ID原有keys
                    if (redisTemplate.delete(str)) {
                        //更新MySql中信息
                        this.cheLiangXinXiMapper.gengXinById(cheLiangXinXi);
                        cheLiangXinXi = cheLiangXinXiMapper.chaXunById(cheLiangXinXi.getId());
                        //获取Redis 中指定车队信息Keys,生成车队信息对象
                        Set<String> cheDuiXinXiKeys = redisTemplate.keys("cheDuiXinXi-" + "*" + "id:" + cheLiangXinXi.getCheDuiXinXiId() + ",*");
                        System.out.println("  cheDuiXinXiKeys =====   " + cheDuiXinXiKeys);
                        List<CheDuiXinXi> cheDuiXinXiList = redisTemplate.opsForValue().multiGet(cheDuiXinXiKeys);
                        for (CheDuiXinXi cheDuiXinXi : cheDuiXinXiList) {
                            System.out.println("cheDuiXinXi  ====   " + cheDuiXinXi);
                            //车队信息对象装进车辆对象生成新的keys并储存到Redis中
                            cheLiangXinXi.setCheDuiXinXi(cheDuiXinXi);
                            if (cheLiangXinXiMapper.chaXunId(cheLiangXinXi.getChePaiHaoMa()) != 0) {
                                redisTemplate.opsForValue().set("cheLiangXinXi-" + "chePaiHaoMa:" + cheLiangXinXi.getChePaiHaoMa() + "," +
                                        "id:" + cheLiangXinXi.getId() + "," + "cheLiangLeiXin:" + cheLiangXinXi.getCheLiangLeiXin() + "," +
                                        "dunWei:" + cheLiangXinXi.getDunWei() + "," + "cheDuiXinXiId:" + cheLiangXinXi.getCheDuiXinXiId() + "," +
                                        "gongZuoZhuangTai:" + cheLiangXinXi.getGongZuoZhuangTai() + "," + "cheLiangShiFouBangDing:" + cheLiangXinXi.getCheLiangShiFouBangDing(), cheLiangXinXi);
                                map.put("code", 200);
                                map.put("msg", "更新成功");
                            } else {
                                map.put("code", 10000);
                                map.put("msg", "更新失败");
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    map.put("code", 100200);
                    map.put("msg", "更新失败");
                }
            }
        } else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(Integer id, String username, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
//        cheLiangXinXiMapper.chaXunById(id);
        Set<String> keys = redisTemplate.keys("cheLiangXinXi-" + "*" + "id:" + id + "" + ",*");
        if(keys.size()!=0){
            for (String str : keys) {
                if (redisTemplate.delete(str)) {
                    this.cheLiangXinXiMapper.shanChuById(id);
                    map.put("code", 200);
                    map.put("msg", "删除成功");

                    XiTongRiZhi xiTongRiZhi = new XiTongRiZhi();
                    xiTongRiZhi.setIp(IPHelper.getIpAddress(request));
                    xiTongRiZhi.setCaoZuoXingWei("删除一条车辆ID:"+ id +"信息");
                    xiTongRiZhi.setUsername(username);
                    xiTongRiZhi.setXieRuShiJian(new Date());
                    xiTongRiZhi.setYiChangXinXi("无");
                    xiTongRiZhi.setYiChangXinXiZhuangTai(0);
                    xiTongRiZhi.setCanShu("code:200");
                    xiTongRiZhiMapper.xinZeng(xiTongRiZhi);
                    break;
                } else {
                    map.put("code", 10000);
                    map.put("msg", "删除失败");
                }
            }
        }else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }
}