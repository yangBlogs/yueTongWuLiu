package com.eihei.yueTongWuLiu.service;

import com.eihei.yueTongWuLiu.pojo.JiaShiYuanXinXi;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 驾驶员信息表(JiaShiYuanXinXi)表服务接口类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:28
 * @version 1.0
 */
public interface JiaShiYuanXinXiService {
    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    Map<String, Object> chaXunCount(String mingCheng);

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    Map<String, Object> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Map<String, Object> chaXunById(Integer id);

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    Map<String, Object> chaXunFenYe(int page,String xingMing,String lianXiDianHua,String shengFenZhengHaoMa,Integer cheDuiXinXiId,String gongZuoZHuangTai);

    /**
     * 新增数据
     *
     * @param jiaShiYuanXinXi 实例对象
     * @return 实例对象
     */
    Map<String, Object> xinZeng(JiaShiYuanXinXi jiaShiYuanXinXi);

    /**
     * 通过ID查询单条数据
     *
     * @param jiaShiYuanXinXi 实例对象
     * @return 实例对象
     */
    Map<String, Object> gengXinById(JiaShiYuanXinXi jiaShiYuanXinXi);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    Map<String, Object> shanChuById(Integer id,String username , HttpServletRequest request);
}