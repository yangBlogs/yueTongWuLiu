package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.XiTongRiZhiMapper;
import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.pojo.ChengYunDan;
import com.eihei.yueTongWuLiu.mapper.ChengYunDanMapper;
import com.eihei.yueTongWuLiu.pojo.User;
import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import com.eihei.yueTongWuLiu.util.IPHelper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.ChengYunDanService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 承运单表(ChengYunDan)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-15 21:33:28
 */
@Service("chengYunDanService")
public class ChengYunDanServiceImpl implements ChengYunDanService {
    @Resource
    private ChengYunDanMapper chengYunDanMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private XiTongRiZhiMapper xiTongRiZhiMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.chengYunDanMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("*chengYunDan-*");
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(keys);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.chengYunDanMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page, String faHuoRen, String shouHuoRen, String wanChengQingKuang, String userId,int id) {
        Map<String, Object> map = new HashMap<>();
        String key = "chengYunDan-*";
        int count = 0;
        boolean faHuoRen_action = false;
        boolean shouHuoRen_action = false;
        boolean wanChengQingKuang_action = false;
        boolean userId_action = false;
        if (faHuoRen != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                faHuoRen_action = true;
                key += "faHuoRen:" + "*" + faHuoRen + "*,shouHuoRen*";
                System.out.println(key);
            }
        }
        if (shouHuoRen != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                shouHuoRen_action = true;
                key += "shouHuoRen:" + "*" + shouHuoRen + "*,wanChengQingKuang*";
                System.out.println(key);
            } else if (count == 2) {
                key = "chengYunDan-*";
                shouHuoRen_action = true;
                key += "faHuoRen:" + "*" + faHuoRen + "*,shouHuoRen:*" + shouHuoRen + "*,wanChengQingKuang*";
                System.out.println(key);
            }
        }
        if (wanChengQingKuang != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                wanChengQingKuang_action = true;
                key += "wanChengQingKuang:" + wanChengQingKuang + ",baoXianFei*";
                System.out.println(key);
            } else if (count == 2) {
                key = "chengYunDan-*";
                wanChengQingKuang_action = true;
                if (faHuoRen != null) {
                    key += "faHuoRen:" + "*" + faHuoRen + "*,*,wanChengQingKuang:" + wanChengQingKuang + ",baoXianFei*";
                } else {
                    key += "shouHuoRen:" + "*" + shouHuoRen + "*,wanChengQingKuang:" + wanChengQingKuang + ",baoXianFei*";
                }
                System.out.println(key);
            } else if (count == 3) {
                key = "chengYunDan-*";
                wanChengQingKuang_action = true;
                key += "faHuoRen:" + "*" + faHuoRen + "*,shouHuoRen:*" + shouHuoRen + "*,wanChengQingKuang:" + wanChengQingKuang + ",baoXianFei*";
                System.out.println(key);
            }
        }
        if (userId != null) {
            count++;
            if (count == 1) {
                userId_action = true;
                key += "userId:" + userId;
                System.out.println(key);
            } else if (count == 2 || count == 3 || count == 4) {
                userId_action = true;
                key += "userId:" + userId;
                System.out.println(key);
            }
        }
        System.out.println("----=" + key);
        Set<String> keys = redisTemplate.keys(key);
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 5 + 5 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 5, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 5, 5);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param chengYunDan 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(ChengYunDan chengYunDan) {
        Map<String, Object> map = new HashMap<>();
        try {
            //通过Redis 查询绑定的user信息对象
            Set<String> userKeys = redisTemplate.keys("user-" + "*" + "id:" + chengYunDan.getUserId() + "" + ",*");
            List<User> userList = redisTemplate.opsForValue().multiGet(userKeys);
            //将user信息对象插入承运单信息中
            for (User user : userList) {
                if (user.getId().equals(chengYunDan.getUserId())) {
                    chengYunDan.setUser(user);
                    chengYunDan.setLuRuShiJian(new Date());
                    chengYunDan.setWanChengQingKuang(0);
                    this.chengYunDanMapper.xinZeng(chengYunDan);
                    int chengYunDanId = chengYunDanMapper.chaXunId(chengYunDan.getFaHuoRen(), chengYunDan.getShouHuoRen(), chengYunDan.getYunFei());
                    chengYunDan.setId(chengYunDanId);
                    redisTemplate.opsForValue().set("chengYunDan-" + "id:" + chengYunDanId + "," + "faHuoRen:" + chengYunDan.getFaHuoRen() + "," + "shouHuoRen:" + chengYunDan.getShouHuoRen() + "," +
                            "wanChengQingKuang:" + chengYunDan.getWanChengQingKuang() + "," + "baoXianFei:" + chengYunDan.getBaoXianFei() + "," + "userId:" + chengYunDan.getUserId(), chengYunDan);
                    map.put("code", 200);
                    map.put("msg", "新增成功");
                    break;
                } else {
                    map.put("code", 10000);
                    map.put("msg", "新增失败，请稍候重试！");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            map.put("code", 100200);
            map.put("msg", "新增失败，请稍候重试！");
        }
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param chengYunDan 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(ChengYunDan chengYunDan) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("chengYunDan-" + "*" + "id:" + chengYunDan.getId() + "" + ",*");
        if (keys.size() != 0) {
            for (String str : keys) {
                try {
                    //删除Redis中此ID原有keys
                    if (redisTemplate.delete(str)) {
                        //更新MySql中信息
                        chengYunDan.setXiuGaiShiJian(new Date());
                        this.chengYunDanMapper.gengXinById(chengYunDan);
                        chengYunDan = chengYunDanMapper.chaXunById(chengYunDan.getId());
                        //获取Redis 中指定user信息Keys,生成user信息对象
                        Set<String> userKeys = redisTemplate.keys("user-" + "*" + "id:" + chengYunDan.getUserId() + "" + ",*");
                        List<User> userList = redisTemplate.opsForValue().multiGet(userKeys);
                        for (User user : userList) {
                            //user信息对象装进承运单信息对象生成新的keys并储存到Redis中
                            chengYunDan.setUser(user);
                            redisTemplate.opsForValue().set("chengYunDan-" + "id:" + chengYunDan.getId() + "," + "faHuoRen:" + chengYunDan.getFaHuoRen() + "," +
                                    "shouHuoRen:" + chengYunDan.getShouHuoRen() + "," + "wanChengQingKuang:" + chengYunDan.getWanChengQingKuang() + "," +
                                    "baoXianFei:" + chengYunDan.getBaoXianFei() + "," + "userId:" + chengYunDan.getUserId(), chengYunDan);
                        }
                        map.put("code", 200);
                        map.put("msg", "更新成功");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    map.put("code", 10000);
                    map.put("msg", "更新失败");
                }
            }
        } else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(Integer id, String username, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("chengYunDan-" + "*" + "id:" + id + "" + ",*");
        if (keys.size() != 0) {
            for (String str : keys) {
                if (redisTemplate.delete(str)) {
                    this.chengYunDanMapper.shanChuById(id);
                    map.put("code", 200);
                    map.put("msg", "删除成功");

                    XiTongRiZhi xiTongRiZhi = new XiTongRiZhi();
                    xiTongRiZhi.setIp(IPHelper.getIpAddress(request));
                    xiTongRiZhi.setCaoZuoXingWei("删除一条承运单ID:"+ id +"信息");
                    xiTongRiZhi.setUsername(username);
                    xiTongRiZhi.setXieRuShiJian(new Date());
                    xiTongRiZhi.setYiChangXinXi("无");
                    xiTongRiZhi.setYiChangXinXiZhuangTai(0);
                    xiTongRiZhi.setCanShu("code:200");
                    xiTongRiZhiMapper.xinZeng(xiTongRiZhi);
                } else {
                    map.put("code", 10000);
                    map.put("msg", "删除失败");
                }
            }
        } else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }
}