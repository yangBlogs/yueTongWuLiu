package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.XiTongRiZhiMapper;
import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.mapper.CheDuiXinXiMapper;
import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import com.eihei.yueTongWuLiu.util.IPHelper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.CheDuiXinXiService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 车队信息(CheDuiXinXi)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-15 21:33:23
 */
@Service("cheDuiXinXiService")
public class CheDuiXinXiServiceImpl implements CheDuiXinXiService {
    @Resource
    private CheDuiXinXiMapper cheDuiXinXiMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private XiTongRiZhiMapper xiTongRiZhiMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.cheDuiXinXiMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("*cheDuiXinXi-*");
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(keys);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.cheDuiXinXiMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page, String cheDuiMingCheng, String cheDuiFuZeRen) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys;

        if (cheDuiMingCheng != null && cheDuiFuZeRen != null) {
            keys = redisTemplate.keys("cheDuiXinXi-" + "*" + "cheDuiMingCheng:" + "*" + cheDuiMingCheng + "*" + "cheDuiFuZeRen:" + "*" + cheDuiFuZeRen + "*" + ",chuangDuiShiJian:*");
            System.out.println("cheDuiXinXi-" + "*" + "cheDuiMingCheng:" + "*" + cheDuiMingCheng + "*" + "cheDuiFuZeRen:" + "*" + cheDuiFuZeRen + "*" + ",chuangDuiShiJian:*");
        } else if (cheDuiMingCheng != null) {
            keys = redisTemplate.keys("cheDuiXinXi-" + "*" + "cheDuiMingCheng:" + "*" + cheDuiMingCheng + "*" + ",id:*");
            System.out.println("cheDuiXinXi-" + "*" + "cheDuiMingCheng:" + "*" + cheDuiMingCheng + "*" + ",id:*");
        } else if (cheDuiFuZeRen != null) {
            keys = redisTemplate.keys("cheDuiXinXi-" + "*" + "cheDuiFuZeRen:" + "*" + cheDuiFuZeRen + "*" + ",chuangDuiShiJian:*");
            System.out.println("cheDuiXinXi-" + "*" + "cheDuiFuZeRen:" + "*" + cheDuiFuZeRen + "*" + ",chuangDuiShiJian:*");
        } else {
            keys = redisTemplate.keys("cheDuiXinXi-" + "*");
            System.out.println("*" + "cheDuiXinXi-" + "*");
        }
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("data", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param cheDuiXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(CheDuiXinXi cheDuiXinXi) {
        Map<String, Object> map = new HashMap<>();
        if (!(" ").equals(cheDuiXinXi.getCheDuiMingCheng())) {
            try {
                Date date = new Date();
                cheDuiXinXi.setChuangDuiShiJian(date);
                this.cheDuiXinXiMapper.xinZeng(cheDuiXinXi);
                int cheDuiXinXiId = cheDuiXinXiMapper.chaXunID(cheDuiXinXi.getCheDuiMingCheng());
                cheDuiXinXi.setId(cheDuiXinXiId);
                if (cheDuiXinXiId != 0) {
                    redisTemplate.opsForValue().set("cheDuiXinXi-" + "cheDuiMingCheng:" + cheDuiXinXi.getCheDuiMingCheng() + "," +
                            "id:" + cheDuiXinXiId + "," + "cheDuiFuZeRen:" + cheDuiXinXi.getCheDuiFuZeRen() + "," +
                            "chuangDuiShiJian:" + date.getTime() + "," + "beiZhu:" + cheDuiXinXi.getBeiZhu(), cheDuiXinXi);
                    map.put("code", 200);
                    map.put("msg", "新增成功");
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 100200);
                map.put("msg", "新增失败，请稍候重试！");
            }
        } else {
            map.put("code", 100200);
            map.put("msg", "新增失败，请填写名称");
        }
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param cheDuiXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(CheDuiXinXi cheDuiXinXi) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("cheDuiXinXi-" + "*" + "id:" + cheDuiXinXi.getId() + "" + ",*");
        if (keys.size() != 0) {
            for (String str : keys) {
                try {
                    //删除Redis中此ID原有keys
                    if (redisTemplate.delete(str)) {
                        //更新MySql中信息
                        this.cheDuiXinXiMapper.gengXinById(cheDuiXinXi);
                        cheDuiXinXi = cheDuiXinXiMapper.chaXunById(cheDuiXinXi.getId());
                        //生成新的Kesy并储存信息
                        redisTemplate.opsForValue().set("cheDuiXinXi-" + "cheDuiMingCheng:" + cheDuiXinXi.getCheDuiMingCheng() + "," +
                                "id:" + cheDuiXinXiMapper.chaXunID(cheDuiXinXi.getCheDuiMingCheng()) + "," + "cheDuiFuZeRen:" + cheDuiXinXi.getCheDuiFuZeRen() + "," +
                                "chuangDuiShiJian:" + cheDuiXinXi.getChuangDuiShiJian().getTime() + "," + "beiZhu:" + cheDuiXinXi.getBeiZhu(), cheDuiXinXi);
                        map.put("code", 200);
                        map.put("msg", "更新成功");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    map.put("code", 100200);
                    map.put("msg", "更新失败");
                }
            }
        } else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(Integer id,String username , HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("cheDuiXinXi-" + "*" + "id:" + id + "" + ",*");
        if(keys.size()!=0){
            for (String str : keys) {
                if (redisTemplate.delete(str)) {
                    this.cheDuiXinXiMapper.shanChuById(id);
                    map.put("code", 200);
                    map.put("msg", "删除成功");

                    XiTongRiZhi xiTongRiZhi = new XiTongRiZhi();
                    xiTongRiZhi.setIp(IPHelper.getIpAddress(request));
                    xiTongRiZhi.setCaoZuoXingWei("删除一条车队ID:"+ id +"信息");
                    xiTongRiZhi.setUsername(username);
                    xiTongRiZhi.setXieRuShiJian(new Date());
                    xiTongRiZhi.setYiChangXinXi("无");
                    xiTongRiZhi.setYiChangXinXiZhuangTai(0);
                    xiTongRiZhi.setCanShu("code:200");
                    xiTongRiZhiMapper.xinZeng(xiTongRiZhi);
                    break;
                } else {
                    map.put("code", 10000);
                    map.put("msg", "删除失败");
                }
            }
        }else {
            map.put("code", 100222);
            map.put("msg", "系统异常,请联系客服");
        }
        return map;
    }
}