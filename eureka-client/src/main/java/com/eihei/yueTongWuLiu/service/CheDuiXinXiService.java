package com.eihei.yueTongWuLiu.service;

import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 车队信息(CheDuiXinXi)表服务接口类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:23
 * @version 1.0
 */
public interface CheDuiXinXiService {
    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    Map<String, Object> chaXunCount(String mingCheng);

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    Map<String, Object> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Map<String, Object> chaXunById(Integer id);

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @param
     * @return 对象列表
     */
    Map<String, Object> chaXunFenYe(int page,  String cheDuiMingCheng,String cheDuiFuZeRen);

    /**
     * 新增数据
     *
     * @param cheDuiXinXi 实例对象
     * @return 实例对象
     */
    Map<String, Object> xinZeng(CheDuiXinXi cheDuiXinXi);

    /**
     * 通过ID查询单条数据
     *
     * @param cheDuiXinXi 实例对象
     * @return 实例对象
     */
    Map<String, Object> gengXinById(CheDuiXinXi cheDuiXinXi);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    Map<String, Object> shanChuById(Integer id,String username, HttpServletRequest request);
}