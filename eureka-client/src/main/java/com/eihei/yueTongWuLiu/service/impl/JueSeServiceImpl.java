package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.pojo.JueSe;
import com.eihei.yueTongWuLiu.mapper.JueSeMapper;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.JueSeService;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色表(JueSe)表服务实现类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:28
 * @version 1.0
 */
@Service("jueSeService")
public class JueSeServiceImpl implements JueSeService {
    @Resource
    private JueSeMapper jueSeMapper;

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
	@Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.jueSeMapper.chaXunAll());
        return map;
    }
}