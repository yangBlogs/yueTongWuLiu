package com.eihei.yueTongWuLiu.service;

import com.eihei.yueTongWuLiu.pojo.JiaShiYuanCheLiang;

import java.util.Map;

/**
 * 驾驶员车辆绑定表(JiaShiYuanCheLiang)表服务接口类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:28
 * @version 1.0
 */
public interface JiaShiYuanCheLiangService {

//    /**
//     * 新增数据
//     *
//     * @param jiaShiYuanCheLiang 实例对象
//     * @return 实例对象
//     */
//    Map<String, Object> bangDingCheLiang(JiaShiYuanCheLiang jiaShiYuanCheLiang);

    /**
     * 驾驶员绑定车辆
     *
     * @return 实例对象
     */
    Map<String, Object> bangDingCheLiang(JiaShiYuanCheLiang jiaShiYuanCheLiang);

    /**
     * 通过主键删除数据
     *
     * @return 是否成功
     */
    Map<String, Object> jieBangCheLiang(Integer id);

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    Map<String, Object> chaXunFenYe(int page);
}