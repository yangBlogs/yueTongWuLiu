package com.eihei.yueTongWuLiu.service;

import com.eihei.yueTongWuLiu.pojo.JueSe;

import java.util.Map;

/**
 * 角色表(JueSe)表服务接口类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:28
 * @version 1.0
 */
public interface JueSeService {
    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    Map<String, Object> chaXunAll();
}