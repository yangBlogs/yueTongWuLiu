package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.CheLiangXinXiMapper;
import com.eihei.yueTongWuLiu.mapper.ChengYunDanMapper;
import com.eihei.yueTongWuLiu.pojo.*;
import com.eihei.yueTongWuLiu.mapper.DiaoDuRenWuMapper;
import org.apache.shiro.crypto.hash.Hash;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.DiaoDuRenWuService;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 调度任务信息表(DiaoDuRenWu)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-15 21:33:28
 */
@Service("diaoDuRenWuService")
public class DiaoDuRenWuServiceImpl implements DiaoDuRenWuService {
    @Resource
    private DiaoDuRenWuMapper diaoDuRenWuMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private CheLiangXinXiServiceImpl cheLiangXinXiServiceImp;
    @Resource
    private ChengYunDanServiceImpl chengYunDanServiceImp;
    @Resource
    private JiaShiYuanXinXiServiceImpl jiaShiYuanXinXiServiceImp;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.diaoDuRenWuMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.diaoDuRenWuMapper.chaXunAll());
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.diaoDuRenWuMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page      查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("diaoDuRenWu-*");
        List<String> key;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key);
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param diaoDuRenWu 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(DiaoDuRenWu diaoDuRenWu) {
        int CheLiangJiaSshiYuanZhuangTai = 1 ;
        int ChengYuanDanZhuangTai = 1;
        return services(diaoDuRenWu,CheLiangJiaSshiYuanZhuangTai,ChengYuanDanZhuangTai);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param diaoDuRenWu 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(DiaoDuRenWu diaoDuRenWu) {
        int CheLiangJiaSshiYuanZhuangTai = 1 ;
        int ChengYuanDanZhuangTai = 1;
        return  services(diaoDuRenWu,CheLiangJiaSshiYuanZhuangTai,ChengYuanDanZhuangTai);
    }

    /**
     * 通过主键删除数据
     *
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(DiaoDuRenWu diaoDuRenWu) {
        int CheLiangJiaSshiYuanZhuangTai = 1 ;
        int ChengYuanDanZhuangTai = 1;
        Map<String,Object> map = new HashMap<>();
        if ((int)services(diaoDuRenWu,CheLiangJiaSshiYuanZhuangTai,ChengYuanDanZhuangTai).get("code") == 200){
            diaoDuRenWuMapper.shanChuById(diaoDuRenWu.getId());
            redisTemplate.delete("diaoDuRenWu-" + "id:" + diaoDuRenWu.getId()+",*");
            map.put("code",200);
            map.put("msg","成功");
        }else {
            map.put("code",10000);
            map.put("msg","失败");
        }
        return map;
    }


    public Map<String, Object> services(DiaoDuRenWu diaoDuRenWu,int CheLiangJiaSshiYuanZhuangTai,int ChengYuanDanZhuangTai) {
        Map<String, Object> map = new HashMap<>();
        if (diaoDuRenWu.getChengYunDanId() != null && diaoDuRenWu.getCheLiangXinXiId() != null) {
            try {
                //修改车辆 状态
                CheLiangXinXi cheLiangXinXi = new CheLiangXinXi();
                cheLiangXinXi.setId(diaoDuRenWu.getCheLiangXinXiId());
                cheLiangXinXi.setGongZuoZhuangTai(CheLiangJiaSshiYuanZhuangTai);
                Map<String, Object> map001 = cheLiangXinXiServiceImp.gengXinById(cheLiangXinXi);
                if ((int) map001.get("code") == 200) {
                    //修改承运单 状态
                    ChengYunDan chengYunDan = new ChengYunDan();
                    chengYunDan.setId(diaoDuRenWu.getChengYunDanId());
                    chengYunDan.setWanChengQingKuang(ChengYuanDanZhuangTai);
                    Map<String, Object> map002 = chengYunDanServiceImp.gengXinById(chengYunDan);
                    if ((int) map002.get("code") == 200) {
                        /*
                         * 通过车辆ID 从jiaShiYuanCheLiang表获取驾驶员ID
                         * 通过驾驶员ID 修改jiaShiYuan工作状态
                         */
                        Set<String> jiaShiYuanCheLiangKeys = redisTemplate.keys("jiaShiYuanCheLiang-*cheLiangXinXiId:" + diaoDuRenWu.getCheLiangXinXiId() +",*");
                        List<JiaShiYuanCheLiang> jiaShiYuanCheLiangList = redisTemplate.opsForValue().multiGet(jiaShiYuanCheLiangKeys);
                        for (JiaShiYuanCheLiang jiaShiYuanCheLiang : jiaShiYuanCheLiangList) {
                            if (jiaShiYuanCheLiang.getCheLiangXinXiId() == diaoDuRenWu.getCheLiangXinXiId()) {
                                int jiaShiYuanId = jiaShiYuanCheLiang.getJiaShiYuanXinXiId();
                                JiaShiYuanXinXi jiaShiYuanXinXi = new JiaShiYuanXinXi();
                                jiaShiYuanXinXi.setId(jiaShiYuanId);
                                jiaShiYuanXinXi.setGongZuoZhuangTai(CheLiangJiaSshiYuanZhuangTai);

                                redisTemplate.delete(jiaShiYuanCheLiangKeys);

                                redisTemplate.opsForValue().set("jiaShiYuanXinXi-" + "xingMing:" + jiaShiYuanXinXi.getXingMing() + "," + "id:" + jiaShiYuanId + "," +
                                        "lianXiDianHua:" + jiaShiYuanXinXi.getLianXiDianHuan() + "," + "shengFenZhengHaoMa:" + jiaShiYuanXinXi.getShengFenZhengHaoMa() + "," +
                                        "cheDuiXinXiId:" + jiaShiYuanXinXi.getCheDuiXinXiId() + "," + "gongZuoZHuangTai:" + CheLiangJiaSshiYuanZhuangTai, jiaShiYuanXinXi);

                                jiaShiYuanXinXiServiceImp.gengXinById(jiaShiYuanXinXi);

                                Date date = new Date();
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if(diaoDuRenWu.getId() == null){
                                    date = dateFormat.parse(dateFormat.format(date));
                                    diaoDuRenWu.setDiaoDuShiJian(date);
                                    this.diaoDuRenWuMapper.xinZeng(diaoDuRenWu);
                                }else {
                                    diaoDuRenWuMapper.gengXinById(diaoDuRenWu);
                                }

                                //获取承运单信息 对象插入调度任务信息
                                Set<String> chengYunDanKeys = redisTemplate.keys("chengYunDan-" + "*" + "id:" + diaoDuRenWu.getChengYunDanId() + "" + ",*");
                                List<ChengYunDan> chengYunDanList = redisTemplate.opsForValue().multiGet(chengYunDanKeys);
                                for (ChengYunDan chengYunDan001 : chengYunDanList) {
                                    redisTemplate.delete(chengYunDanKeys);

                                    chengYunDan001.setUser(null);
                                    redisTemplate.opsForValue().set("chengYunDan-" + "id:" + chengYunDan001.getId() + "," + "faHuoRen:" + chengYunDan.getShouHuoRen() + "," + "shouHuoRen:" + chengYunDan.getShouHuoRen() + "," +
                                            "wanChengQingKuang:" + ChengYuanDanZhuangTai + "," + "baoXianFei:" + chengYunDan.getBaoXianFei() + "," + "userId:" + chengYunDan.getUserId(), chengYunDan001);

                                    diaoDuRenWu.setChengYunDan(chengYunDan001);
                                }
                                //获取车辆信息对象 插入调度任务信息
                                Set<String> cheLiangXinXiKeys = redisTemplate.keys("cheLiangXinXi-" + "*" + "id:" + diaoDuRenWu.getCheLiangXinXiId() + "" + ",*");
                                List<CheLiangXinXi> cheLiangXinXiList = redisTemplate.opsForValue().multiGet(cheLiangXinXiKeys);
                                for (CheLiangXinXi cheLiangXinXi001 : cheLiangXinXiList) {
                                    System.out.println(cheLiangXinXi001.toString());
                                    redisTemplate.delete(cheLiangXinXiKeys);

                                    cheLiangXinXi001.setCheDuiXinXi(null);
                                    redisTemplate.opsForValue().set("cheLiangXinXi-" + "chePaiHaoMa:" + cheLiangXinXi.getChePaiHaoMa() + "," +
                                            "id:" + cheLiangXinXi001.getId() + "," + "cheLiangLeiXin:" + cheLiangXinXi.getCheLiangLeiXin() + "," +
                                            "dunWei:" + cheLiangXinXi.getDunWei() + "," + "cheDuiXinXiId:" + cheLiangXinXi.getCheDuiXinXiId() + "," +
                                            "gongZuoZhuangTai:" + CheLiangJiaSshiYuanZhuangTai + "," + "cheLiangShiFouBangDing:" + cheLiangXinXi.getCheLiangShiFouBangDing(), cheLiangXinXi001);


                                    diaoDuRenWu.setCheLiangXinXi(cheLiangXinXi001);
                                }
                                //获取用户信息对象 插入调度任务信息
                                Set<String> userKeys = redisTemplate.keys("user-" + "*" + "id:" + diaoDuRenWu.getUserId() + "" + ",*");
                                List<User> userList = redisTemplate.opsForValue().multiGet(userKeys);
                                for (User user : userList) {
                                    user.setJueSe(null);
                                    diaoDuRenWu.setUser(user);
                                }
                                //获取sql中调度任务Id
                                System.out.println("shijian    ==  " + dateFormat.format(date));
                                int diaoDuRenWuId = diaoDuRenWuMapper.chaXunId(diaoDuRenWu.getChengYunDanId(), diaoDuRenWu.getCheLiangXinXiId(), dateFormat.format(date)).getId();
                                System.out.println("     == " + diaoDuRenWuId);

                                diaoDuRenWu.setId(diaoDuRenWuId);
                                if (diaoDuRenWu.getId() != null) {
                                    redisTemplate.delete("diaoDuRenWu-" + "id:" + diaoDuRenWu.getId()+",*");

                                    redisTemplate.opsForValue().set("diaoDuRenWu-" + "id:" + diaoDuRenWu.getId() + "," +
                                            "chengYunDanId:" + diaoDuRenWu.getChengYunDanId() + "," +
                                            "cheLiangXinXiId:" + diaoDuRenWu.getCheLiangXinXiId(), diaoDuRenWu);
                                    map.put("code", 200);
                                    map.put("msg", "新增成功");
                                } else {
                                    map.put("code", 100222);
                                    map.put("msg", "新增失败，请稍候重试！");
                                }
                                break;
                            } else {
                                map.put("code", 100223);
                                map.put("msg", "新增失败，请稍候重试！");
                            }
                        }
                    } else {
                        map.put("code", 100223);
                        map.put("msg", "新增失败，请稍候重试！");
                    }
                } else {
                    map.put("code", 100224);
                    map.put("msg", "新增失败，请稍候重试！");
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 100200);
                map.put("msg", "新增失败，请稍候重试！");
            }
        } else {
            map.put("code", 10000);
            map.put("msg", "新增失败,请填写必填项");
        }
        return map;
    }
}