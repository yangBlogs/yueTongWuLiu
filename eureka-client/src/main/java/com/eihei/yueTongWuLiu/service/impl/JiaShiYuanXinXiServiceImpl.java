package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.JiaShiYuanCheLiangMapper;
import com.eihei.yueTongWuLiu.mapper.XiTongRiZhiMapper;
import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.pojo.JiaShiYuanCheLiang;
import com.eihei.yueTongWuLiu.pojo.JiaShiYuanXinXi;
import com.eihei.yueTongWuLiu.mapper.JiaShiYuanXinXiMapper;
import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import com.eihei.yueTongWuLiu.util.IPHelper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.JiaShiYuanXinXiService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 驾驶员信息表(JiaShiYuanXinXi)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-15 21:33:28
 */
@Service("jiaShiYuanXinXiService")
public class JiaShiYuanXinXiServiceImpl implements JiaShiYuanXinXiService {
    @Resource
    private JiaShiYuanXinXiMapper jiaShiYuanXinXiMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private JiaShiYuanCheLiangMapper jiaShiYuanCheLiangMapper;
    @Resource
    private XiTongRiZhiMapper xiTongRiZhiMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.jiaShiYuanXinXiMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("jiaShiYuanXinXi-*");
        List<JiaShiYuanXinXi> list = redisTemplate.opsForValue().multiGet(keys);
        System.out.println("==================================="+list.size());
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.jiaShiYuanXinXiMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page, String xingMing, String lianXiDianHua, String shengFenZhengHaoMa, Integer cheDuiXinXiId, String gongZuoZHuangTai) {
        Map<String, Object> map = new HashMap<>();
        String key = "jiaShiYuanXinXi-*";
        int count = 0;
        boolean xingMing_action = false;
        boolean lianXiDianHua_action = false;
        boolean shengFenZhengHaoMa_action = false;
        boolean cheDuiXinXiId_action = false;
        boolean gongZuoZHuangTai_action = false;
        if (xingMing != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                xingMing_action = true;
                key += "xingMing:" + "*" + xingMing + "*,id:*";
                System.out.println(key);
            }
        }
        if (lianXiDianHua != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                lianXiDianHua_action = true;
                key += "lianXiDianHua:" + "*" + lianXiDianHua + "*,shengFenZhengHaoMa:*";
                System.out.println(key);
            } else if (count == 2) {
                key = "jiaShiYuanXinXi-*";
                lianXiDianHua_action = true;
                key += "xingMing:" + "*" + xingMing + "*,id:*" + ",lianXiDianHua:*" + lianXiDianHua + "*,shengFenZhengHaoMa:*";
                System.out.println(key);
            }
        }
        if (shengFenZhengHaoMa != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                shengFenZhengHaoMa_action = true;
                key += "shengFenZhengHaoMa:" + "*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:*";
                System.out.println(key);
            } else if (count == 2) {
                key = "jiaShiYuanXinXi-*";
                shengFenZhengHaoMa_action = true;
                if (xingMing != null) {
                    key += "xingMing:" + "*" + xingMing + "*,id:*" + ",*,shengFenZhengHaoMa:*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:*";
                } else {
                    key += "lianXiDianHua:" + "*" + lianXiDianHua + "*,shengFenZhengHaoMa:*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:*";
                }
                System.out.println(key);
            } else if (count == 3) {
                key = "jiaShiYuanXinXi-*";
                shengFenZhengHaoMa_action = true;
                key += "xingMing:" + "*" + xingMing + "*,id:*" + ",lianXiDianHua:*" + lianXiDianHua + "*,shengFenZhengHaoMa:*" +
                        shengFenZhengHaoMa + "*,cheDuiXinXiId:*";
                System.out.println(key);
            }
        }
        if (cheDuiXinXiId != null) {
            count++;
            if (count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                cheDuiXinXiId_action = true;
                key += "cheDuiXinXiId:" + cheDuiXinXiId + ",gongZuoZHuangTai:*";
                System.out.println(key);
            } else if (count == 2) {
                key = "jiaShiYuanXinXi-*";
                cheDuiXinXiId_action = true;
                if (xingMing != null) {
                    key += "xingMing:" + "*" + xingMing + "*,id:*,*,*,cheDuiXinXiId:" + cheDuiXinXiId + ",gongZuoZHuangTai:*";
                } else if (lianXiDianHua != null) {
                    key += "lianXiDianHua:" + "*" + lianXiDianHua + "*,*,cheDuiXinXiId:" + cheDuiXinXiId + ",gongZuoZHuangTai:*";
                } else {
                    key += "shengFenZhengHaoMa:" + "*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:" + cheDuiXinXiId + ",gongZuoZHuangTai:*";
                }
                System.out.println(key);
            } else if (count == 3) {
                key = "jiaShiYuanXinXi-*";
                cheDuiXinXiId_action = true;
                if (xingMing != null && lianXiDianHua != null) {
                    key += "xingMing:" + "*" + xingMing + "*,id:*" + ",lianXiDianHua:*" + lianXiDianHua + "*,*,cheDuiXinXiId:" +
                            cheDuiXinXiId + ",gongZuoZHuangTai:*";
                } else if (xingMing != null && shengFenZhengHaoMa != null) {
                    key += "xingMing:" + "*" + xingMing + "*,id:*" + ",*,shengFenZhengHaoMa:*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:" +
                            cheDuiXinXiId + ",gongZuoZHuangTai:*";
                } else {
                    key += "lianXiDianHua:*" + lianXiDianHua + "*,shengFenZhengHaoMa:*" + shengFenZhengHaoMa + "*,cheDuiXinXiId:" +
                            cheDuiXinXiId + ",gongZuoZHuangTai:*";
                }
                System.out.println(key);
            } else if (count == 4) {
                key = "jiaShiYuanXinXi-*";
                cheDuiXinXiId_action = true;
                key += "xingMing:" + "*" + xingMing + "*,id:*" + ",lianXiDianHua:*" + lianXiDianHua + "*,shengFenZhengHaoMa:*" +
                        shengFenZhengHaoMa + "*,cheDuiXinXiId:" + cheDuiXinXiId + ",gongZuoZHuangTai:*";
                System.out.println(key);
            }
        }
        if (gongZuoZHuangTai != null) {
            count++;
            if (count == 1) {
                gongZuoZHuangTai_action = true;
                key += "gongZuoZHuangTai:" + gongZuoZHuangTai;
                System.out.println(key);
            } else if (count == 2 || count == 3 || count == 4 || count ==5) {
                gongZuoZHuangTai_action = true;
                if (cheDuiXinXiId != null){
                    key += gongZuoZHuangTai;
                }else {
                    key += ",gongZuoZHuangTai:" + gongZuoZHuangTai;
                }
                System.out.println(key);
            }
        }
        System.out.println("----=" + key);
        Set<String> keys = redisTemplate.keys(key);
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param jiaShiYuanXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(JiaShiYuanXinXi jiaShiYuanXinXi) {
        Map<String, Object> map = new HashMap<>();
        if (jiaShiYuanXinXi.getXingMing() != null && jiaShiYuanXinXi.getShengFenZhengHaoMa() != null) {
            try {

                jiaShiYuanXinXi.setJiaRuShiJian(new Date());

                this.jiaShiYuanXinXiMapper.xinZeng(jiaShiYuanXinXi);
                int jiaShiYuanXinXiId = jiaShiYuanXinXiMapper.chaXunId(jiaShiYuanXinXi.getShengFenZhengHaoMa());
                jiaShiYuanXinXi.setId(jiaShiYuanXinXiId);
                if (jiaShiYuanXinXiId != 0) {
                    redisTemplate.opsForValue().set("jiaShiYuanXinXi-" + "xingMing:" + jiaShiYuanXinXi.getXingMing() + "," + "id:" + jiaShiYuanXinXiId + "," +
                            "lianXiDianHua:" + jiaShiYuanXinXi.getLianXiDianHuan() + "," + "shengFenZhengHaoMa:" + jiaShiYuanXinXi.getShengFenZhengHaoMa() + "," +
                            "cheDuiXinXiId:" + jiaShiYuanXinXi.getCheDuiXinXiId() + "," + "gongZuoZHuangTai:" + jiaShiYuanXinXi.getGongZuoZhuangTai(), jiaShiYuanXinXi);

                    //为驾驶员车辆绑定表增加驾驶员信息
                    jiaShiYuanCheLiangMapper.xinZeng(jiaShiYuanXinXiId);
                    JiaShiYuanCheLiang jiaShiYuanCheLiang = new JiaShiYuanCheLiang();
                    jiaShiYuanCheLiang.setId(jiaShiYuanCheLiangMapper.chaXunId(jiaShiYuanXinXiId).getId());
                    jiaShiYuanCheLiang.setJiaShiYuanXinXiId(jiaShiYuanXinXiId);
                    jiaShiYuanCheLiang.setJiaShiYuanXinXi(jiaShiYuanXinXiMapper.chaXunById(jiaShiYuanXinXiId));
                    redisTemplate.opsForValue().set("jiaShiYuanCheLiang-" + "jiaShiYuanXinXiId:" + jiaShiYuanXinXiId + "," +
                            "id:" + jiaShiYuanCheLiang.getId(), jiaShiYuanCheLiang);
                    map.put("code", 200);
                    map.put("msg", "新增成功");
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 100200);
                map.put("msg", "新增失败，请稍候重试！");
            }
        } else {
            map.put("code", 10000);
            map.put("msg", "新增失败,填写必填项");
        }
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param jiaShiYuanXinXi 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(JiaShiYuanXinXi jiaShiYuanXinXi) {

        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("jiaShiYuanXinXi-" + "*" + "id:" + jiaShiYuanXinXi.getId() + "" + ",*");
        for (String str : keys) {
            try {
                if (redisTemplate.delete(str)) {
                    this.jiaShiYuanXinXiMapper.gengXinById(jiaShiYuanXinXi);
                    Set<String> cheDuiXinXiKeys = redisTemplate.keys("cheDuiXinXi-" + "*" + "id:" + jiaShiYuanXinXi.getCheDuiXinXiId() + "" + ",*");
                    List<CheDuiXinXi> cheDuiXinXiList = redisTemplate.opsForValue().multiGet(cheDuiXinXiKeys);
                    for (CheDuiXinXi cheDuiXinXi : cheDuiXinXiList) {
                        jiaShiYuanXinXi = jiaShiYuanXinXiMapper.chaXunById(jiaShiYuanXinXi.getId());
                        System.out.println("dasdasfafaf=asf=sa=f=asf=sa=fasf=="  + jiaShiYuanXinXi.toString());
                        //车队信息对象装进车辆对象生成新的keys并储存到Redis中
                        jiaShiYuanXinXi.setCheDuiXinXi(cheDuiXinXi);
                        if (jiaShiYuanXinXiMapper.chaXunId(jiaShiYuanXinXi.getShengFenZhengHaoMa()) != 0) {
                            redisTemplate.opsForValue().set("jiaShiYuanXinXi-" + "xingMing:" + jiaShiYuanXinXi.getXingMing() + "," + "id:" + jiaShiYuanXinXi.getId() + "," +
                                    "lianXiDianHua:" + jiaShiYuanXinXi.getLianXiDianHuan() + "," + "shengFenZhengHaoMa:" + jiaShiYuanXinXi.getShengFenZhengHaoMa() + "," +
                                    "cheDuiXinXiId:" + jiaShiYuanXinXi.getCheDuiXinXiId() + "," + "gongZuoZHuangTai:" + jiaShiYuanXinXi.getGongZuoZhuangTai(), jiaShiYuanXinXi);
                        }
                    }
                    map.put("code", 200);
                    map.put("msg", "更新成功");
                }
            } catch (Exception e) {
                map.put("code", 10000);
                map.put("msg", "更新失败");
            }
        }
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(Integer id,String username , HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("jiaShiYuanXinXi-" + "*" + "id:" + id + "" + ",*");
        for (String str : keys) {
            if (redisTemplate.delete(str)) {
                this.jiaShiYuanXinXiMapper.shanChuById(id);
                map.put("code", 200);
                map.put("msg", "删除成功");
                XiTongRiZhi xiTongRiZhi = new XiTongRiZhi();
                xiTongRiZhi.setIp(IPHelper.getIpAddress(request));
                xiTongRiZhi.setCaoZuoXingWei("删除一条车队ID:"+ id +"信息");
                xiTongRiZhi.setUsername(username);
                xiTongRiZhi.setXieRuShiJian(new Date());
                xiTongRiZhi.setYiChangXinXi("无");
                xiTongRiZhi.setYiChangXinXiZhuangTai(0);
                xiTongRiZhi.setCanShu("code:200");
                xiTongRiZhiMapper.xinZeng(xiTongRiZhi);
                break;
            } else {
                map.put("code", 10000);
                map.put("msg", "删除失败");
            }
        }
        return map;
    }
}