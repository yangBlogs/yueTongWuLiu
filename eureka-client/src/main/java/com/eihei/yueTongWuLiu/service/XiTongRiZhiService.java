package com.eihei.yueTongWuLiu.service;

import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;

import java.util.Map;

/**
 * 系统日志表(XiTongRiZhi)表服务接口类
 *
 * @author yangToT
 * @since 2021-03-25 09:52:10
 * @version 1.0
 */
public interface XiTongRiZhiService {
    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    Map<String, Object> chaXunCount(String caoZuoXingWei,String username,String xieRuShiJian_KaiSHi,String xieRuShiJian_JieShu);

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    Map<String, Object> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Map<String, Object> chaXunById(Integer id);

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    Map<String, Object> chaXunFenYe(int page, String caoZuoXingWei,String username,String xieRuShiJian_KaiSHi,String xieRuShiJian_JieShu );

    /**
     * 新增数据
     *
     * @param xiTongRiZhi 实例对象
     * @return 实例对象
     */
    Map<String, Object> xinZeng(XiTongRiZhi xiTongRiZhi);


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    Map<String, Object> shanChuById(String id);
}