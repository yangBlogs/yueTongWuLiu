package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.XiTongRiZhiMapper;
import com.eihei.yueTongWuLiu.pojo.*;
import com.eihei.yueTongWuLiu.mapper.HuoWuMapper;
import com.eihei.yueTongWuLiu.util.IPHelper;
import com.netflix.ribbon.proxy.annotation.Http;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.HuoWuService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 货物表(HuoWu)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-18 21:17:09
 */
@Service("huoWuService")
public class HuoWuServiceImpl implements HuoWuService {
    @Resource
    private HuoWuMapper huoWuMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private XiTongRiZhiMapper xiTongRiZhiMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    @Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.huoWuMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     *
     * @return 返回所有数据
     */
    @Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.huoWuMapper.chaXunAll());
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.huoWuMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page      查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page, String huoWuMingCheng,String chengYunDanId) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys;
        if (huoWuMingCheng == null && chengYunDanId == null){
            keys = redisTemplate.keys("huoWu-*");
        }else if(huoWuMingCheng == null){
            keys = redisTemplate.keys("huoWu-*" + "chengYunDanId:" + chengYunDanId);
        }else if (chengYunDanId == null){
            keys = redisTemplate.keys("huoWu-"+"*" + "huoWuMingCheng:" + "*" + huoWuMingCheng + "*"+ ",id:*");
        }else {
            keys = redisTemplate.keys("huoWu-*" + "huoWuMingCheng:*" + huoWuMingCheng + "*" + "chengYunDanId:" + chengYunDanId);
        }
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 5 + 5 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 5, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 5, 5);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param huoWu 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> xinZeng(HuoWu huoWu) {
        Map<String, Object> map = new HashMap<>();
        if (huoWu.getHuoWuMingCheng() != null && huoWu.getChengYunDanId() != null) {
            try {
                Set<String> chengYunDanKeys = redisTemplate.keys("chengYunDan-" + "*" + "id:" + huoWu.getChengYunDanId() + "" + ",*");
                List<ChengYunDan> chengYunDanList = redisTemplate.opsForValue().multiGet(chengYunDanKeys);
                for (ChengYunDan chengYunDan : chengYunDanList) {
                    huoWu.setChengYunDan(chengYunDan);
                    this.huoWuMapper.xinZeng(huoWu);
                    huoWu.setId(huoWuMapper.chaXunId(huoWu.getHuoWuMingCheng(), huoWu.getChengYunDanId()).getId());
                        System.out.println("id ===================" + huoWu.getId());
                        redisTemplate.opsForValue().set("huoWu-" + "huoWuMingCheng:" + huoWu.getHuoWuMingCheng() + "," +
                                "id:" + huoWu.getId() + "," + "chengYunDanId:" + huoWu.getChengYunDanId(), huoWu);
                        map.put("code", 200);
                        map.put("msg", "新增成功");
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 100200);
                map.put("msg", "新增失败，请稍候重试！");
            }
        } else {
            map.put("code", 10000);
            map.put("msg", "新增失败,填写必填项");
        }
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param huoWu 实例对象
     * @return 实例对象
     */
    @Override
    public Map<String, Object> gengXinById(HuoWu huoWu) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("huoWu-" + "*" + "id:" + huoWu.getId() + "" + ",*");
        for (String str : keys) {
            try {
                //删除Redis中此ID原有keys
                if (redisTemplate.delete(str)) {
                    //更新MySql中信息
                    this.huoWuMapper.gengXinById(huoWu);
                    //获取Redis 中指定承运单信息Keys,生成承运单信息对象
                    Set<String> chengYunDanKeys = redisTemplate.keys("chengYunDan-" + "*" + "id:" + huoWu.getChengYunDanId() + "" + ",*");
                    List<ChengYunDan> chengYunDanList = redisTemplate.opsForValue().multiGet(chengYunDanKeys);
                    for (ChengYunDan chengYunDan : chengYunDanList) {
                        huoWu = huoWuMapper.chaXunById(huoWu.getId());
                        huoWu.setChengYunDan(chengYunDan);
                        redisTemplate.opsForValue().set("huoWu-" + "huoWuMingCheng:" + huoWu.getHuoWuMingCheng() + "," +
                                "id:" + huoWu.getId() + "," + "chengYunDanId:" + huoWu.getChengYunDanId(), huoWu);
                        map.put("code", 200);
                        map.put("msg", "更新成功");
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                map.put("code", 10000);
                map.put("msg", "更新失败");
            }
        }
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> shanChuById(String id, String username, HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("huoWu-" + "*" + "id:" + id + "" + ",*");
        for (String str : keys) {
            if (redisTemplate.delete(str)) {
                this.huoWuMapper.shanChuById(id);
                map.put("code", 200);
                map.put("msg", "删除成功");

                XiTongRiZhi xiTongRiZhi = new XiTongRiZhi();
                xiTongRiZhi.setIp(IPHelper.getIpAddress(request));
                xiTongRiZhi.setCaoZuoXingWei("删除一条货物ID:"+ id +"信息");
                xiTongRiZhi.setUsername(username);
                xiTongRiZhi.setXieRuShiJian(new Date());
                xiTongRiZhi.setYiChangXinXi("无");
                xiTongRiZhi.setYiChangXinXiZhuangTai(0);
                xiTongRiZhi.setCanShu("code:200");
                xiTongRiZhiMapper.xinZeng(xiTongRiZhi);
                break;
            } else {
                map.put("code", 10000);
                map.put("msg", "删除失败");
            }
        }
        return map;
    }

}