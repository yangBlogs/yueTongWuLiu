package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.JueSeMapper;
import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.pojo.JueSe;
import com.eihei.yueTongWuLiu.pojo.User;
import com.eihei.yueTongWuLiu.mapper.UserMapper;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.UserService;
import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.*;

/**
 * 用户(User)表服务实现类
 *
 * @author yangToT
 * @since 2021-03-15 21:33:28
 * @version 1.0
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserMapper userMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private JueSeMapper jueSeMapper;

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
	@Override
    public Map<String, Object> chaXunCount(String mingCheng) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", this.userMapper.chaXunCount(mingCheng));
        return map;
    }

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
	@Override
    public Map<String, Object> chaXunAll() {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("*user-*");
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(keys);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        return map;
    }

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
	@Override
    public Map<String, Object> chaXunById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        // 前端端分离时，前端人员会首先判断code值是否满足200，如果不是200，则提醒用户失败
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("obj", this.userMapper.chaXunById(id));
        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
	@Override
    public Map<String, Object> chaXunFenYe(int page, String username,String zhangHao,String phone,Integer jueSeId) {
        Map<String, Object> map = new HashMap<>();
        String key = "user-*";
        int count = 0;
        boolean username_action = false;
        boolean zhangHao_action = false;
        boolean phone_action = false;
        boolean jueSeId_action = false;
        if(username != null) {
            count++;
            if(count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                username_action = true;
                key += "username:" + "*" + username + "*,zhangHao*";
                System.out.println(key);
            }
        }
        if(zhangHao != null) {
            count++;
            if(count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                zhangHao_action = true;
                key += "zhangHao:" + "*" + zhangHao + "*,phone*";
                System.out.println(key);
            }
            else if(count == 2) {
                key = "user-*";
                zhangHao_action = true;
                key += "username:" + "*" + username + "*,zhangHao:*" + zhangHao + "*,phone:*";
                System.out.println(key);
            }
        }
        if(phone != null) {
            count++;
            if(count == 1) {  // 当count==1时，代表用户传递用户名查询，所以需要拼接sql查询用户名称
                phone_action = true;
                key += "phone:" + "*" + phone + "*,id*";
                System.out.println(key);
            }
            else if(count == 2) {
                key = "user-*";
                phone_action = true;
                if (zhangHao != null){
                    key += "zhangHao:" + "*" + zhangHao + "*,phone:*" + phone + "*,id:*";
                }else {
                    key += "username:" + "*" + username + "*,zhangHao*,phone:*" + phone + "*,id:*";
                }
                System.out.println(key);
            }
            else if(count == 3) {
                key = "user-*";
                phone_action = true;
                key += "username:" + "*" + username + "*,zhangHao:*" + zhangHao + "*,phone:*" + phone + "*,id:*";
                System.out.println(key);
            }
        }
        if (jueSeId != null){
            count++;
            if (count == 1){
                jueSeId_action = true;
                key += "jueSeId:" + jueSeId;
                System.out.println(key);
            }else if(count == 2 || count == 3 || count == 4) {
                jueSeId_action = true;
                key += "jueSeId:" + jueSeId;
                System.out.println(key);
            }
        }
        System.out.println("----=" + key);
        Set<String> keys =  redisTemplate.keys(key);
        List<String> key01;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key01 = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key01 = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key01);
        map.put("code", 200);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
	@Override
    public Map<String, Object> xinZeng(User user) {
        Map<String, Object> map = new HashMap<>();
	    if (user.getUsername() != null && user.getZhangHao() != null && user.getPassword() != null){
            String mima = user.getPassword();
            String yongHuZhangHao = user.getZhangHao();
            user.setPassword(md5(yongHuZhangHao,mima));
            user.setChuangJianShiJian(new Date());
            this.userMapper.xinZeng(user);
            //通过角色Id 查询角色信息
            user.setJueSe(jueSeMapper.chaXunById(user.getJueSeId()));
            //在Sql中查询新增数据Id 添加到 key中
           int userId = userMapper.chaXunId(user.getPhone());
           user.setId(userId);
           redisTemplate.opsForValue().set("user-" + "username:" + user.getUsername() + "," + "zhangHao:" + yongHuZhangHao + "," + "phone:" + user.getPhone() + "," +
                                              "id:" + userId + "," + "jueSeId:" + user.getJueSeId(),user);
           map.put("code",200);
           map.put("msg","新增成功");
        }else {
            map.put("code", 10000);
            map.put("msg", "新增失败,填写必填项");
        }
        return map;
    }

    // 注册时，进行shiro加密，返回加密后的结果，如果在加入shiro之前，存在用户密码不是此方式加密的，那么将无法登录
    // 使用用户账号作为盐值
    private String md5(String username, String password){
        String hashAlgorithmName = "MD5";                   // 加密方式
        ByteSource salt = ByteSource.Util.bytes(username);  // 以账号作为盐值
        int hashIterations = 11;                            // 加密11次
        return new SimpleHash(hashAlgorithmName, password, salt, hashIterations).toString();
    }

    /**
     * 通过ID查询单条数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
	@Override
    public Map<String, Object> gengXinById(User user) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("user-"+"*" + "id:" + user.getId() + "" + ",*");
        for (String str : keys){
            try {
                //删除Redis 原有信息
                if(redisTemplate.delete(str)){
                    String mima = user.getPassword();
                    String yongHuZhangHao = user.getZhangHao();
                    user.setPassword(md5(yongHuZhangHao,mima));
                    //更新MySql中信息
                    this.userMapper.gengXinById(user);
                    //获取更新后user信息对象
                    user = userMapper.chaXunById(user.getId());
                    //获取角色信息对象 增加到user对象中
                    user.setJueSe(jueSeMapper.chaXunById(user.getJueSeId()));
                    redisTemplate.opsForValue().set("user-" + "username:" + user.getUsername() + "," + "zhangHao:" + user.getZhangHao() + "," + "phone:" + user.getPhone() + "," +
                            "id:" + user.getId() + "," + "jueSeId:" + user.getJueSeId(),user);
                    map.put("code", 200);
                    map.put("msg", "更新成功");
                }
            }catch (Exception e){
                map.put("code", 10000);
                map.put("msg", "更新失败");
            }
        }
        map.put("code", 200);
        map.put("msg", "更新成功");
        return map;
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
	@Override
    public Map<String, Object> shanChuById(Integer id) {
        Map<String, Object> map = new HashMap<>();
        //获取Redis中此ID信息的Keys
        Set<String> keys = redisTemplate.keys("user-"+"*" + "id:" + id + "" + ",*");
	    for (String str : keys){
	        //删除Redis中数据
	        if (redisTemplate.delete(str)){
	            //删除sql中数据
                this.userMapper.shanChuById(id);
                map.put("code", 200);
                map.put("msg", "删除成功");
            }else {
                map.put("code", 10000);
                map.put("msg", "删除失败");
            }
        }
        return map;
    }
}