package com.eihei.yueTongWuLiu.service.impl;

import com.eihei.yueTongWuLiu.mapper.CheLiangXinXiMapper;
import com.eihei.yueTongWuLiu.pojo.*;
import com.eihei.yueTongWuLiu.mapper.JiaShiYuanCheLiangMapper;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.eihei.yueTongWuLiu.service.JiaShiYuanCheLiangService;


import javax.annotation.Resource;
import java.util.*;

/**
 * 驾驶员车辆绑定表(JiaShiYuanCheLiang)表服务实现类
 *
 * @author yangToT
 * @version 1.0
 * @since 2021-03-15 21:33:28
 */
@Service("jiaShiYuanCheLiangService")
public class JiaShiYuanCheLiangServiceImpl implements JiaShiYuanCheLiangService {
    @Resource
    private JiaShiYuanCheLiangMapper jiaShiYuanCheLiangMapper;
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private CheLiangXinXiMapper cheLiangXinXiMapper;
    @Resource
    private CheLiangXinXiServiceImpl cheLiangXinXiServiceImp;

    /**
     * 为驾驶员绑定车辆信息
     *
     * @param jiaShiYuanCheLiang 实例对象
     * @return 实例对象
     */
//    @Override
//    public Map<String, Object> bangDingCheLiang(JiaShiYuanCheLiang jiaShiYuanCheLiang) {
//        Map<String, Object> map = new HashMap<>();
//        if (jiaShiYuanCheLiang.getCheLiangXinXiId() != null && jiaShiYuanCheLiang.getJiaShiYuanXinXiId() != null) {
//            try {
//                this.jiaShiYuanCheLiangMapper.bangDingCheLiang(jiaShiYuanCheLiang);
//                int jiaShiYuanCheLiangId = jiaShiYuanCheLiangMapper.chaXunID(jiaShiYuanCheLiang);
//                jiaShiYuanCheLiang.setId(jiaShiYuanCheLiangId);
//                if (jiaShiYuanCheLiangId != 0) {
//                    redisTemplate.opsForValue().set("jiaShiYuanCheLiang-" + "jiaShiYuanXinXiId:" + jiaShiYuanCheLiang.getJiaShiYuanXinXiId() + "," +
//                                                        "cheLiangXinXiId:" + jiaShiYuanCheLiang.getCheLiangXinXiId() + "," + "id:" + jiaShiYuanCheLiang.getId(),jiaShiYuanCheLiang);
//                    map.put("code", 200);
//                    map.put("msg", "绑定成功");
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                map.put("code", 100200);
//                map.put("msg", "绑定失败，请稍候重试！");
//            }
//        } else {
//            map.put("code", 10000);
//            map.put("msg", "请选择必选项");
//        }
//        return map;
//    }

    /**
     * 通过ID查询单条数据
     *
     * @return 实例对象
     */
    @Override
    public Map<String, Object> bangDingCheLiang(JiaShiYuanCheLiang jiaShiYuanCheLiang) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("jiaShiYuanCheLiang-" + "*" + "id:" + jiaShiYuanCheLiang.getId());
        for (String str : keys) {
            try {
                //删除Redis中原有Key
                if (redisTemplate.delete(str)) {
                    if (this.jiaShiYuanCheLiangMapper.bangDingCheLiang(jiaShiYuanCheLiang) != 0) {
                        //查出Redis中车辆信息对象
                        Set<String> cheLiangXinXiKeys = redisTemplate.keys("cheLiangXinXi-" + "*" + "id:" + jiaShiYuanCheLiang.getCheLiangXinXiId() + "" + ",*");
                        List<CheLiangXinXi> cheLiangXinXiList = redisTemplate.opsForValue().multiGet(cheLiangXinXiKeys);
                        CheLiangXinXi cheLiangXinXi = new CheLiangXinXi();
                        for (CheLiangXinXi cheLiangXinXi1 : cheLiangXinXiList) {
                            cheLiangXinXi = cheLiangXinXi1;
                        }
//                        //查出Redis中车驾驶员息对象
                        int jiaShiYuanCheLiangID = jiaShiYuanCheLiang.getId();
                        JiaShiYuanCheLiang jiaShiYuanCheLiang1 = jiaShiYuanCheLiangMapper.chaXunById(jiaShiYuanCheLiangID);
                        System.out.println("===-=-=-=--=-" + jiaShiYuanCheLiang1.toString());
                        int jiaShiYuanId = jiaShiYuanCheLiang1.getJiaShiYuanXinXiId();
                        System.out.println("-=-=-=-=-=" + jiaShiYuanId);

                        Set<String> jiaShiYuanXinXiKeys = redisTemplate.keys("jiaShiYuanXinXi-" + "*" + "id:" + jiaShiYuanId + "" + ",*");
                        List<JiaShiYuanXinXi> jiaShiYuanXinXiList = redisTemplate.opsForValue().multiGet(jiaShiYuanXinXiKeys);
                        JiaShiYuanXinXi jiaShiYuanXinXi = new JiaShiYuanXinXi();
                        for (JiaShiYuanXinXi jiaShiYuanXinXi1 : jiaShiYuanXinXiList) {
                            jiaShiYuanXinXi = jiaShiYuanXinXi1;
                            System.out.println("===================================================" + jiaShiYuanXinXi);
                        }

                        //修改车辆信息中 是否绑定 为 1:已绑定
                        cheLiangXinXi.setCheLiangShiFouBangDing(1);
                        cheLiangXinXiMapper.gengXinById(cheLiangXinXi);

                        //更新Redis中车辆信息K,V
                        Map<String, Object> map1 = cheLiangXinXiServiceImp.gengXinById(cheLiangXinXi);
                        if ((int) map1.get("code") == 200) {
                            //将 驾驶员信息对象 与 车辆信息对象 注入到驾驶员车辆绑定对象中
                            jiaShiYuanCheLiang.setJiaShiYuanXinXiId(jiaShiYuanId);
                            jiaShiYuanCheLiang.setJiaShiYuanXinXi(jiaShiYuanXinXi);
                            jiaShiYuanCheLiang.setCheLiangXinXi(cheLiangXinXi);
                            System.out.println(" ===============" + jiaShiYuanId);
                            redisTemplate.opsForValue().set("jiaShiYuanCheLiang-" + "jiaShiYuanXinXiId:" + jiaShiYuanId + "," + "cheLiangXinXiId:" + jiaShiYuanCheLiang.getCheLiangXinXiId() + ",id:" + jiaShiYuanCheLiang.getId(), jiaShiYuanCheLiang);
                            map.put("code", 200);
                            map.put("msg", "绑定成功");
                        }
                    }
                }
            } catch (Exception e) {
                map.put("code", 10000);
                map.put("msg", "绑定失败");
            }
        }
        return map;
    }

    /**
     * 通过驾驶员id解绑车辆信息
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public Map<String, Object> jieBangCheLiang(Integer id) {
        System.out.println(id);
        Map<String, Object> map = new HashMap<>();
        JiaShiYuanCheLiang jiaShiYuanCheLiang = jiaShiYuanCheLiangMapper.chaXunById(id);
        Set<String> jiaShiYuanXinXiKeys = redisTemplate.keys("jiaShiYuanXinXi-" + "*" + "id:" + jiaShiYuanCheLiang.getJiaShiYuanXinXiId() + "" + ",*");
        List<JiaShiYuanXinXi> jiaShiYuanXinXiList = redisTemplate.opsForValue().multiGet(jiaShiYuanXinXiKeys);
        for (JiaShiYuanXinXi jiaShiYuanXinXi : jiaShiYuanXinXiList) {
            Set<String> keys = redisTemplate.keys("jiaShiYuanCheLiang-" + "*" + "jiaShiYuanXinXiId:" + jiaShiYuanCheLiang.getJiaShiYuanXinXiId() + ",*");
            for (String str : keys) {
                jiaShiYuanCheLiang = new JiaShiYuanCheLiang();
                this.jiaShiYuanCheLiangMapper.jieBangCheLiang(id);
                jiaShiYuanCheLiang.setId(id);
                jiaShiYuanCheLiang.setJiaShiYuanXinXi(jiaShiYuanXinXi);
                jiaShiYuanCheLiang.setJiaShiYuanXinXiId(jiaShiYuanXinXi.getId());
                redisTemplate.opsForValue().set(str, jiaShiYuanCheLiang);
                map.put("code", 200);
                map.put("msg", "成功");
            }
        }


        return map;
    }

    /**
     * 查询分页数据
     *
     * @param page 查询起始位置
     * @return 对象列表
     */
    @Override
    public Map<String, Object> chaXunFenYe(int page) {
        Map<String, Object> map = new HashMap<>();
        Set<String> keys = redisTemplate.keys("jiaShiYuanCheLiang-*");
        List<String> key;
        List<String> keys_list = new ArrayList<String>(keys);
        if ((page - 1) * 10 + 10 > keys.size()) {
            key = keys_list.subList((page - 1) * 10, keys_list.size());
        } else {
            key = keys_list.subList((page - 1) * 10, 10);
        }
        List<CheDuiXinXi> list = redisTemplate.opsForValue().multiGet(key);
        map.put("code", 0);
        map.put("msg", "查询成功");
        map.put("list", list);
        map.put("count", keys.size());
        return map;
    }
}