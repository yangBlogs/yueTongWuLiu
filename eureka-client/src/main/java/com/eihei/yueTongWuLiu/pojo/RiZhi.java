package com.eihei.yueTongWuLiu.pojo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 日志字典(RiZhi)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "RiZhi",description = "日志字典")
public class RiZhi implements Serializable {
    private static final long serialVersionUID = -35288967586476913L;
    /**
    * 类型Id
    */
	@ApiModelProperty(name = "id",notes = "类型Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 类型名称
    */
	@ApiModelProperty(name = "leiXinMingCheng",notes = "类型名称",dataType = "String",required = true)
    private String leiXinMingCheng;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getLeiXinMingCheng() {
        return leiXinMingCheng;
    }

    public void setLeiXinMingCheng(String leiXinMingCheng) {
        this.leiXinMingCheng = leiXinMingCheng;
    }

}