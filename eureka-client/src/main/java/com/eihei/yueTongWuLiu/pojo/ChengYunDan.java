package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 承运单表(ChengYunDan)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "ChengYunDan",description = "承运单表")
public class ChengYunDan implements Serializable {
    private static final long serialVersionUID = -84784901001049350L;
    /**
    * 承运单表 ID

    */
	@ApiModelProperty(name = "id",notes = "承运单表 ID ",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 发货单位
    */
	@ApiModelProperty(name = "faHuoDanWei",notes = "发货单位",dataType = "String",required = true)
    private String faHuoDanWei;
    /**
    * 发货单位地址
    */
	@ApiModelProperty(name = "faHuoDanWeiDiZhi",notes = "发货单位地址",dataType = "String",required = true)
    private String faHuoDanWeiDiZhi;
    /**
    * 发货人
    */
	@ApiModelProperty(name = "faHuoRen",notes = "发货人",dataType = "String",required = true)
    private String faHuoRen;
    /**
    * 发货人电话
    */
	@ApiModelProperty(name = "faHuoRenDianHua",notes = "发货人电话",dataType = "String",required = true)
    private String faHuoRenDianHua;
    /**
    * 收货单位
    */
	@ApiModelProperty(name = "shouHuoDanWei",notes = "收货单位",dataType = "String",required = true)
    private String shouHuoDanWei;
    /**
    * 收货单位地址
    */
	@ApiModelProperty(name = "shouHuoDanWeiDiZhi",notes = "收货单位地址",dataType = "String",required = true)
    private String shouHuoDanWeiDiZhi;
    /**
    * 收货人
    */
	@ApiModelProperty(name = "shouHuoRen",notes = "收货人",dataType = "String",required = true)
    private String shouHuoRen;
    /**
    * 收货人电话
    */
	@ApiModelProperty(name = "shouHuoRenDianHua",notes = "收货人电话",dataType = "String",required = true)
    private String shouHuoRenDianHua;
    /**
    * 承运日期
    */
	@ApiModelProperty(name = "chengYunRiQi",notes = "承运日期",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chengYunRiQi;
    /**
    * 收货时间
    */
	@ApiModelProperty(name = "shouHuoShiJian",notes = "收货时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date shouHuoShiJian;
    /**
    * 完成情况：0:待调度 1:已调度 2:已签收
3:已结算
    */
	@ApiModelProperty(name = "wanChengQingKuang",notes = "完成情况：0:待调度 1:已调度 2:已签收 3:已结算",dataType = "Integer",required = true)
    private Integer wanChengQingKuang;
    /**
    * 保险费
    */
	@ApiModelProperty(name = "baoXianFei",notes = "保险费",dataType = "String",required = true)
    private String baoXianFei;
    /**
    * 运费
    */
	@ApiModelProperty(name = "yunFei",notes = "运费",dataType = "String",required = true)
    private String yunFei;
    /**
    * 其他费用
    */
	@ApiModelProperty(name = "qiTaFeiYong",notes = "其他费用",dataType = "String",required = true)
    private String qiTaFeiYong;
    /**
    * 合计费用
    */
	@ApiModelProperty(name = "heJiFeiYong",notes = "合计费用",dataType = "String",required = true)
    private String heJiFeiYong;
    /**
    * 备注
    */
	@ApiModelProperty(name = "beiZhu",notes = "备注",dataType = "String",required = true)
    private String beiZhu;
    /**
    * 业务员信息Id
    */
	@ApiModelProperty(name = "userId",notes = "业务员信息Id",dataType = "Integer",required = true)
    private Integer userId;
    /**
    * 录入时间
    */
	@ApiModelProperty(name = "luRuShiJian",notes = "录入时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date luRuShiJian;
    /**
    * 数据记录状态 : 0:使用中	1:该记录已删
除
    */
	@ApiModelProperty(name = "shuJuJiLiZhuangTai",notes = "数据记录状态 : 0:使用中	1:该记录已删除",dataType = "Integer",required = true)
    private Integer shuJuJiLiZhuangTai;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

	private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getFaHuoDanWei() {
        return faHuoDanWei;
    }

    public void setFaHuoDanWei(String faHuoDanWei) {
        this.faHuoDanWei = faHuoDanWei;
    }
        
    public String getFaHuoDanWeiDiZhi() {
        return faHuoDanWeiDiZhi;
    }

    public void setFaHuoDanWeiDiZhi(String faHuoDanWeiDiZhi) {
        this.faHuoDanWeiDiZhi = faHuoDanWeiDiZhi;
    }
        
    public String getFaHuoRen() {
        return faHuoRen;
    }

    public void setFaHuoRen(String faHuoRen) {
        this.faHuoRen = faHuoRen;
    }
        
    public String getFaHuoRenDianHua() {
        return faHuoRenDianHua;
    }

    public void setFaHuoRenDianHua(String faHuoRenDianHua) {
        this.faHuoRenDianHua = faHuoRenDianHua;
    }
        
    public String getShouHuoDanWei() {
        return shouHuoDanWei;
    }

    public void setShouHuoDanWei(String shouHuoDanWei) {
        this.shouHuoDanWei = shouHuoDanWei;
    }
        
    public String getShouHuoDanWeiDiZhi() {
        return shouHuoDanWeiDiZhi;
    }

    public void setShouHuoDanWeiDiZhi(String shouHuoDanWeiDiZhi) {
        this.shouHuoDanWeiDiZhi = shouHuoDanWeiDiZhi;
    }
        
    public String getShouHuoRen() {
        return shouHuoRen;
    }

    public void setShouHuoRen(String shouHuoRen) {
        this.shouHuoRen = shouHuoRen;
    }
        
    public String getShouHuoRenDianHua() {
        return shouHuoRenDianHua;
    }

    public void setShouHuoRenDianHua(String shouHuoRenDianHua) {
        this.shouHuoRenDianHua = shouHuoRenDianHua;
    }
        
    public Date getChengYunRiQi() {
        return chengYunRiQi;
    }

    public void setChengYunRiQi(Date chengYunRiQi) {
        this.chengYunRiQi = chengYunRiQi;
    }
        
    public Date getShouHuoShiJian() {
        return shouHuoShiJian;
    }

    public void setShouHuoShiJian(Date shouHuoShiJian) {
        this.shouHuoShiJian = shouHuoShiJian;
    }
        
    public Integer getWanChengQingKuang() {
        return wanChengQingKuang;
    }

    public void setWanChengQingKuang(Integer wanChengQingKuang) {
        this.wanChengQingKuang = wanChengQingKuang;
    }


    public String getBaoXianFei() {
        return baoXianFei;
    }

    public void setBaoXianFei(String baoXianFei) {
        this.baoXianFei = baoXianFei;
    }

    public String getYunFei() {
        return yunFei;
    }

    public void setYunFei(String yunFei) {
        this.yunFei = yunFei;
    }

    public String getQiTaFeiYong() {
        return qiTaFeiYong;
    }

    public void setQiTaFeiYong(String qiTaFeiYong) {
        this.qiTaFeiYong = qiTaFeiYong;
    }

    public String getHeJiFeiYong() {
        return heJiFeiYong;
    }

    public void setHeJiFeiYong(String heJiFeiYong) {
        this.heJiFeiYong = heJiFeiYong;
    }

    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
        
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
        
    public Date getLuRuShiJian() {
        return luRuShiJian;
    }

    public void setLuRuShiJian(Date luRuShiJian) {
        this.luRuShiJian = luRuShiJian;
    }
        
    public Integer getShuJuJiLiZhuangTai() {
        return shuJuJiLiZhuangTai;
    }

    public void setShuJuJiLiZhuangTai(Integer shuJuJiLiZhuangTai) {
        this.shuJuJiLiZhuangTai = shuJuJiLiZhuangTai;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }

}