package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 用户(User)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "User",description = "用户")
public class User implements Serializable {
    private static final long serialVersionUID = 608850463119912923L;
    /**
    * 用户Id
    */
	@ApiModelProperty(name = "id",notes = "用户Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 用户姓名
    */
	@ApiModelProperty(name = "username",notes = "用户姓名",dataType = "String",required = true)
    private String username;
    /**
    * 性别 0:女  1:男
    */
	@ApiModelProperty(name = "sex",notes = "性别 0:女  1:男",dataType = "Integer",required = true)
    private Integer sex;
    /**
    * 用户账号
    */
	@ApiModelProperty(name = "zhangHao",notes = "用户账号",dataType = "String",required = true)
    private String zhangHao;
    /**
    * 用户密码
    */
	@ApiModelProperty(name = "password",notes = "用户密码",dataType = "String",required = true)
    private String password;
    /**
    * 联系电话
    */
	@ApiModelProperty(name = "phone",notes = "联系电话",dataType = "String",required = true)
    private String phone;
    /**
    * 电子邮箱
    */
	@ApiModelProperty(name = "email",notes = "电子邮箱",dataType = "String",required = true)
    private String email;
    /**
    * 权限Id
    */
	@ApiModelProperty(name = "jueSeId",notes = "权限Id",dataType = "Integer",required = true)
    private Integer jueSeId;
    /**
    * 用户创建时间
    */
	@ApiModelProperty(name = "chuangJianShiJian",notes = "用户创建时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chuangJianShiJian;
    /**
    * 数据记录状态 0:使用中 1:该记录已删
除
    */
	@ApiModelProperty(name = "shuJuJiLuZhuangTai",notes = "数据记录状态 0:使用中 1:该记录已删除",dataType = "Integer",required = true)
    private Integer shuJuJiLuZhuangTai;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

	private JueSe jueSe;

    public JueSe getJueSe() {
        return jueSe;
    }

    public void setJueSe(JueSe jueSe) {
        this.jueSe = jueSe;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
        
    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }
        
    public String getZhangHao() {
        return zhangHao;
    }

    public void setZhangHao(String zhangHao) {
        this.zhangHao = zhangHao;
    }
        
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
        
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
        
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        
    public Integer getJueSeId() {
        return jueSeId;
    }

    public void setJueSeId(Integer jueSeId) {
        this.jueSeId = jueSeId;
    }
        
    public Date getChuangJianShiJian() {
        return chuangJianShiJian;
    }

    public void setChuangJianShiJian(Date chuangJianShiJian) {
        this.chuangJianShiJian = chuangJianShiJian;
    }
        
    public Integer getShuJuJiLuZhuangTai() {
        return shuJuJiLuZhuangTai;
    }

    public void setShuJuJiLuZhuangTai(Integer shuJuJiLuZhuangTai) {
        this.shuJuJiLuZhuangTai = shuJuJiLuZhuangTai;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }

}