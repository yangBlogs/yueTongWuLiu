package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 驾驶员信息表(JiaShiYuanXinXi)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "JiaShiYuanXinXi",description = "驾驶员信息表")
public class JiaShiYuanXinXi implements Serializable {
    private static final long serialVersionUID = -76449670586465088L;
    /**
    * 驾驶员Id
    */
	@ApiModelProperty(name = "id",notes = "驾驶员Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 驾驶员姓名
    */
	@ApiModelProperty(name = "xingMing",notes = "驾驶员姓名",dataType = "String",required = true)
    private String xingMing;
    /**
    * 性别 ： 0 男 1 女
    */
	@ApiModelProperty(name = "xingBie",notes = "性别 ： 0 男 1 女",dataType = "Integer",required = true)
    private Integer xingBie;
    /**
    * 出生日期
    */
	@ApiModelProperty(name = "chuShengRiQi",notes = "出生日期",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd", timezone = "GMT+8")
    private Date chuShengRiQi;
    /**
    * 联系电话
    */
	@ApiModelProperty(name = "lianXiDianHuan",notes = "联系电话",dataType = "String",required = true)
    private String lianXiDianHuan;
    /**
    * 身份证号码
    */
	@ApiModelProperty(name = "shengFenZhengHaoMa",notes = "身份证号码",dataType = "String",required = true)
    private String shengFenZhengHaoMa;
    /**
    * 车队信息Id
    */
	@ApiModelProperty(name = "cheDuiXinXiId",notes = "车队信息Id",dataType = "Integer",required = true)
    private Integer cheDuiXinXiId;
    /**
    * 工作状态 :	1:承运中 2:空闲
    */
	@ApiModelProperty(name = "gongZuoZhuangTai",notes = "工作状态 :	1:承运中 2:空闲",dataType = "Integer",required = true)
    private Integer gongZuoZhuangTai;
    /**
    * 备注
    */
	@ApiModelProperty(name = "beiZhu",notes = "备注",dataType = "String",required = true)
    private String beiZhu;
    /**
    * 加入时间
    */
	@ApiModelProperty(name = "jiaRuShiJian",notes = "加入时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jiaRuShiJian;
    /**
    * 数据记录状态 : 0:使用中	1:该记录已删
除
    */
	@ApiModelProperty(name = "shuJuJiLuZhuangTai",notes = "数据记录状态 : 0:使用中	1:该记录已删除",dataType = "Integer",required = true)
    private Integer shuJuJiLuZhuangTai;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

	private CheDuiXinXi cheDuiXinXi;

    public CheDuiXinXi getCheDuiXinXi() {
        return cheDuiXinXi;
    }

    public void setCheDuiXinXi(CheDuiXinXi cheDuiXinXi) {
        this.cheDuiXinXi = cheDuiXinXi;
    }

    private JiaShiYuanXinXi jiaShiYuanXinXi;

    public JiaShiYuanXinXi getJiaShiYuanXinXi() {
        return jiaShiYuanXinXi;
    }

    public void setJiaShiYuanXinXi(JiaShiYuanXinXi jiaShiYuanXinXi) {
        this.jiaShiYuanXinXi = jiaShiYuanXinXi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getXingMing() {
        return xingMing;
    }

    public void setXingMing(String xingMing) {
        this.xingMing = xingMing;
    }
        
    public Integer getXingBie() {
        return xingBie;
    }

    public void setXingBie(Integer xingBie) {
        this.xingBie = xingBie;
    }
        
    public Date getChuShengRiQi() {
        return chuShengRiQi;
    }

    public void setChuShengRiQi(Date chuShengRiQi) {
        this.chuShengRiQi = chuShengRiQi;
    }
        
    public String getLianXiDianHuan() {
        return lianXiDianHuan;
    }

    public void setLianXiDianHuan(String lianXiDianHuan) {
        this.lianXiDianHuan = lianXiDianHuan;
    }
        
    public String getShengFenZhengHaoMa() {
        return shengFenZhengHaoMa;
    }

    public void setShengFenZhengHaoMa(String shengFenZhengHaoMa) {
        this.shengFenZhengHaoMa = shengFenZhengHaoMa;
    }
        
    public Integer getCheDuiXinXiId() {
        return cheDuiXinXiId;
    }

    public void setCheDuiXinXiId(Integer cheDuiXinXiId) {
        this.cheDuiXinXiId = cheDuiXinXiId;
    }
        
    public Integer getGongZuoZhuangTai() {
        return gongZuoZhuangTai;
    }

    public void setGongZuoZhuangTai(Integer gongZuoZhuangTai) {
        this.gongZuoZhuangTai = gongZuoZhuangTai;
    }
        
    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
        
    public Date getJiaRuShiJian() {
        return jiaRuShiJian;
    }

    public void setJiaRuShiJian(Date jiaRuShiJian) {
        this.jiaRuShiJian = jiaRuShiJian;
    }
        
    public Integer getShuJuJiLuZhuangTai() {
        return shuJuJiLuZhuangTai;
    }

    public void setShuJuJiLuZhuangTai(Integer shuJuJiLuZhuangTai) {
        this.shuJuJiLuZhuangTai = shuJuJiLuZhuangTai;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }


    @Override
    public String toString() {
        return "JiaShiYuanXinXi{" +
                "id=" + id +
                ", xingMing='" + xingMing + '\'' +
                ", xingBie=" + xingBie +
                ", chuShengRiQi=" + chuShengRiQi +
                ", lianXiDianHuan='" + lianXiDianHuan + '\'' +
                ", shengFenZhengHaoMa='" + shengFenZhengHaoMa + '\'' +
                ", cheDuiXinXiId=" + cheDuiXinXiId +
                ", gongZuoZhuangTai=" + gongZuoZhuangTai +
                ", beiZhu='" + beiZhu + '\'' +
                ", jiaRuShiJian=" + jiaRuShiJian +
                ", shuJuJiLuZhuangTai=" + shuJuJiLuZhuangTai +
                ", xiuGaiShiJian=" + xiuGaiShiJian +
                ", cheDuiXinXi=" + cheDuiXinXi +
                ", jiaShiYuanXinXi=" + jiaShiYuanXinXi +
                '}';
    }
}