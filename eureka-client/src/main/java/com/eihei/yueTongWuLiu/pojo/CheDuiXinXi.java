package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 车队信息(CheDuiXinXi)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:22
 * @version 1.0
 */
@ApiModel(value = "CheDuiXinXi",description = "车队信息")
public class CheDuiXinXi implements Serializable {
    private static final long serialVersionUID = 953032509449464060L;
    /**
    * 车队编号（字段自动编号）
    */
	@ApiModelProperty(name = "id",notes = "车队编号（字段自动编号）",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 车队名称
    */
	@ApiModelProperty(name = "cheDuiMingCheng",notes = "车队名称",dataType = "String",required = true)
    private String cheDuiMingCheng;
    /**
    * 车队负责人
    */
	@ApiModelProperty(name = "cheDuiFuZeRen",notes = "车队负责人",dataType = "String",required = true)
    private String cheDuiFuZeRen;
    /**
    * 备注
    */
	@ApiModelProperty(name = "beiZhu",notes = "备注",dataType = "String",required = true)
    private String beiZhu;
    /**
    * 创队时间
    */
	@ApiModelProperty(name = "chuangDuiShiJian",notes = "创队时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chuangDuiShiJian;
    /**
    * 车队状态 0:使用中 1:该记录已删除
    */
	@ApiModelProperty(name = "shuJuJiLuZhuangTai",notes = "车队状态 0:使用中 1:该记录已删除",dataType = "Integer",required = true)
    private Integer shuJuJiLuZhuangTai;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getCheDuiMingCheng() {
        return cheDuiMingCheng;
    }

    public void setCheDuiMingCheng(String cheDuiMingCheng) {
        this.cheDuiMingCheng = cheDuiMingCheng;
    }
        
    public String getCheDuiFuZeRen() {
        return cheDuiFuZeRen;
    }

    public void setCheDuiFuZeRen(String cheDuiFuZeRen) {
        this.cheDuiFuZeRen = cheDuiFuZeRen;
    }
        
    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
        
    public Date getChuangDuiShiJian() {
        return chuangDuiShiJian;
    }

    public void setChuangDuiShiJian(Date chuangDuiShiJian) {
        this.chuangDuiShiJian = chuangDuiShiJian;
    }
        
    public Integer getShuJuJiLuZhuangTai() {
        return shuJuJiLuZhuangTai;
    }

    public void setShuJuJiLuZhuangTai(Integer shuJuJiLuZhuangTai) {
        this.shuJuJiLuZhuangTai = shuJuJiLuZhuangTai;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }

    @Override
    public String toString() {
        return "CheDuiXinXi{" +
                "id=" + id +
                ", cheDuiMingCheng='" + cheDuiMingCheng + '\'' +
                ", cheDuiFuZeRen='" + cheDuiFuZeRen + '\'' +
                ", beiZhu='" + beiZhu + '\'' +
                ", chuangDuiShiJian=" + chuangDuiShiJian +
                ", shuJuJiLuZhuangTai=" + shuJuJiLuZhuangTai +
                ", xiuGaiShiJian=" + xiuGaiShiJian +
                '}';
    }
}