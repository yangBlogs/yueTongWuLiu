package com.eihei.yueTongWuLiu.pojo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 角色表(JueSe)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "JueSe",description = "角色表")
public class JueSe implements Serializable {
    private static final long serialVersionUID = 651991872413576944L;
    /**
    * 角色Id
    */
	@ApiModelProperty(name = "id",notes = "角色Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 角色名称

    */
	@ApiModelProperty(name = "jueSeMingCheng",notes = "角色名称 ",dataType = "String",required = true)
    private String jueSeMingCheng;
    /**
    * 角色权限
    */
	@ApiModelProperty(name = "jueSeQuanXian",notes = "角色权限",dataType = "Integer",required = true)
    private Integer jueSeQuanXian;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getJueSeMingCheng() {
        return jueSeMingCheng;
    }

    public void setJueSeMingCheng(String jueSeMingCheng) {
        this.jueSeMingCheng = jueSeMingCheng;
    }
        
    public Integer getJueSeQuanXian() {
        return jueSeQuanXian;
    }

    public void setJueSeQuanXian(Integer jueSeQuanXian) {
        this.jueSeQuanXian = jueSeQuanXian;
    }

}