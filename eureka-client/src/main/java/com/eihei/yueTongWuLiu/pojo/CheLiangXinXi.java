package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 车辆信息表(CheLiangXinXi)实体类
 *
 * @author yangToT
 * @since 2021-03-17 10:14:08
 * @version 1.0
 */
@ApiModel(value = "CheLiangXinXi",description = "车辆信息表")
public class CheLiangXinXi implements Serializable {

    /**
    * 车辆信息Id
    */
	@ApiModelProperty(name = "id",notes = "车辆信息Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 车牌号码
    */
	@ApiModelProperty(name = "chePaiHaoMa",notes = "车牌号码",dataType = "String",required = true)
    private String chePaiHaoMa;
    /**
    * 购车日期
    */
	@ApiModelProperty(name = "gouCheRiQi",notes = "购车日期",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date gouCheRiQi;
    /**
    * 车辆类型
    */
	@ApiModelProperty(name = "cheLiangLeiXin",notes = "车辆类型",dataType = "String",required = true)
    private String cheLiangLeiXin;
    /**
    * 创建时间
    */
	@ApiModelProperty(name = "chuangJianShiJian",notes = "创建时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chuangJianShiJian;
    /**
    * 吨位
    */
	@ApiModelProperty(name = "dunWei",notes = "吨位",dataType = "Integer",required = true)
    private Integer dunWei;
    /**
    * 所属车队编号 外键
    */
	@ApiModelProperty(name = "cheDuiXinXiId",notes = "所属车队编号 外键",dataType = "Integer",required = true)
    private Integer cheDuiXinXiId;
    /**
    * 工作状态 :	1:承运中 2:空闲
    */
	@ApiModelProperty(name = "gongZuoZhuangTai",notes = "工作状态 :	1:承运中 2:空闲",dataType = "Integer",required = true)
    private Integer gongZuoZhuangTai;
    /**
    * 备注
    */
	@ApiModelProperty(name = "beiZhu",notes = "备注",dataType = "String",required = true)
    private String beiZhu;
    /**
    * 加入时间
    */
	@ApiModelProperty(name = "jiaRuShiJian",notes = "加入时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date jiaRuShiJian;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

    /**
     * 车辆是否被绑定
     */
    @ApiModelProperty(name = "cheLiangShiFouBangDing",notes = "车辆是否被绑定 1:已绑定",dataType = "Integer",required = true)
    private Integer cheLiangShiFouBangDing;

    public Integer getCheLiangShiFouBangDing() {
        return cheLiangShiFouBangDing;
    }

    public void setCheLiangShiFouBangDing(Integer cheLiangShiFouBangDing) {
        this.cheLiangShiFouBangDing = cheLiangShiFouBangDing;
    }

    public CheDuiXinXi getCheDuiXinXi() {
        return cheDuiXinXi;
    }

    public void setCheDuiXinXi(CheDuiXinXi cheDuiXinXi) {
        this.cheDuiXinXi = cheDuiXinXi;
    }

    private CheDuiXinXi cheDuiXinXi;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getChePaiHaoMa() {
        return chePaiHaoMa;
    }

    public void setChePaiHaoMa(String chePaiHaoMa) {
        this.chePaiHaoMa = chePaiHaoMa;
    }
        
    public Date getGouCheRiQi() {
        return gouCheRiQi;
    }

    public void setGouCheRiQi(Date gouCheRiQi) {
        this.gouCheRiQi = gouCheRiQi;
    }
        
    public String getCheLiangLeiXin() {
        return cheLiangLeiXin;
    }

    public void setCheLiangLeiXin(String cheLiangLeiXin) {
        this.cheLiangLeiXin = cheLiangLeiXin;
    }
        
    public Date getChuangJianShiJian() {
        return chuangJianShiJian;
    }

    public void setChuangJianShiJian(Date chuangJianShiJian) {
        this.chuangJianShiJian = chuangJianShiJian;
    }
        
    public Integer getDunWei() {
        return dunWei;
    }

    public void setDunWei(Integer dunWei) {
        this.dunWei = dunWei;
    }
        
    public Integer getCheDuiXinXiId() {
        return cheDuiXinXiId;
    }

    public void setCheDuiXinXiId(Integer cheDuiXinXiId) {
        this.cheDuiXinXiId = cheDuiXinXiId;
    }
        
    public Integer getGongZuoZhuangTai() {
        return gongZuoZhuangTai;
    }

    public void setGongZuoZhuangTai(Integer gongZuoZhuangTai) {
        this.gongZuoZhuangTai = gongZuoZhuangTai;
    }
        
    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
        
    public Date getJiaRuShiJian() {
        return jiaRuShiJian;
    }

    public void setJiaRuShiJian(Date jiaRuShiJian) {
        this.jiaRuShiJian = jiaRuShiJian;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }


    @Override
    public String toString() {
        return "CheLiangXinXi{" +
                "id=" + id +
                ", chePaiHaoMa='" + chePaiHaoMa + '\'' +
                ", gouCheRiQi=" + gouCheRiQi +
                ", cheLiangLeiXin='" + cheLiangLeiXin + '\'' +
                ", chuangJianShiJian=" + chuangJianShiJian +
                ", dunWei=" + dunWei +
                ", cheDuiXinXiId=" + cheDuiXinXiId +
                ", gongZuoZhuangTai=" + gongZuoZhuangTai +
                ", beiZhu='" + beiZhu + '\'' +
                ", jiaRuShiJian=" + jiaRuShiJian +
                ", xiuGaiShiJian=" + xiuGaiShiJian +
                ", cheLiangShiFouBangDing=" + cheLiangShiFouBangDing +
                ", cheDuiXinXi=" + cheDuiXinXi +
                '}';
    }
}