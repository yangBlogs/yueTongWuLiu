package com.eihei.yueTongWuLiu.pojo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 车队运输成本(YunShuChenBen)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "YunShuChenBen",description = "车队运输成本")
public class YunShuChenBen implements Serializable {
    private static final long serialVersionUID = 955636712038028831L;
    /**
    * 成本信息Id
    */
	@ApiModelProperty(name = "id",notes = "成本信息Id",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 车辆信息Id
    */
	@ApiModelProperty(name = "cheLiangXinXiId",notes = "车辆信息Id",dataType = "Integer",required = true)
    private Integer cheLiangXinXiId;
    /**
    * 车队信息Id
    */
	@ApiModelProperty(name = "cheDuiXinXiId",notes = "车队信息Id",dataType = "Integer",required = true)
    private Integer cheDuiXinXiId;
    /**
    * 驾驶员信息Id
    */
	@ApiModelProperty(name = "jiaShiYuanXinXiId",notes = "驾驶员信息Id",dataType = "Integer",required = true)
    private Integer jiaShiYuanXinXiId;
    /**
    * 业务员Id 
    */
	@ApiModelProperty(name = "userId",notes = "业务员Id ",dataType = "Integer",required = true)
    private Integer userId;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public Integer getCheLiangXinXiId() {
        return cheLiangXinXiId;
    }

    public void setCheLiangXinXiId(Integer cheLiangXinXiId) {
        this.cheLiangXinXiId = cheLiangXinXiId;
    }
        
    public Integer getCheDuiXinXiId() {
        return cheDuiXinXiId;
    }

    public void setCheDuiXinXiId(Integer cheDuiXinXiId) {
        this.cheDuiXinXiId = cheDuiXinXiId;
    }
        
    public Integer getJiaShiYuanXinXiId() {
        return jiaShiYuanXinXiId;
    }

    public void setJiaShiYuanXinXiId(Integer jiaShiYuanXinXiId) {
        this.jiaShiYuanXinXiId = jiaShiYuanXinXiId;
    }
        
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

}