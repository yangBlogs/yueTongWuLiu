package com.eihei.yueTongWuLiu.pojo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 货物表(HuoWu)实体类
 *
 * @author yangToT
 * @since 2021-03-18 21:17:09
 * @version 1.0
 */
@ApiModel(value = "HuoWu",description = "货物表")
public class HuoWu implements Serializable {
    private static final long serialVersionUID = 846242074477225151L;
    /**
    * 货物ID
    */
	@ApiModelProperty(name = "id",notes = "货物ID",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 货物名称
    */
	@ApiModelProperty(name = "huoWuMingCheng",notes = "货物名称",dataType = "String",required = true)
    private String huoWuMingCheng;
    /**
    * 货物数量
    */
	@ApiModelProperty(name = "huoWuShuLiang",notes = "货物数量",dataType = "Integer",required = true)
    private Integer huoWuShuLiang;
    /**
    * 货物重量
    */
	@ApiModelProperty(name = "huoWuZhongLiang",notes = "货物重量",dataType = "String",required = true)
    private String huoWuZhongLiang;
    /**
    * 货物体积
    */
	@ApiModelProperty(name = "huoWuTiJi",notes = "货物体积",dataType = "String",required = true)
    private String huoWuTiJi;
    /**
    * 承运单ID
    */
	@ApiModelProperty(name = "chengYunDanId",notes = "承运单ID",dataType = "Integer",required = true)
    private Integer chengYunDanId;
    /**
    * 数据记录状态 : 0:使用中	1:该记录已删
除
    */
	@ApiModelProperty(name = "shuJuJiLuZhuangTai",notes = "数据记录状态 : 0:使用中	1:该记录已删 除",dataType = "Integer",required = true)
    private Integer shuJuJiLuZhuangTai;

	private ChengYunDan chengYunDan;

    public ChengYunDan getChengYunDan() {
        return chengYunDan;
    }

    public void setChengYunDan(ChengYunDan chengYunDan) {
        this.chengYunDan = chengYunDan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getHuoWuMingCheng() {
        return huoWuMingCheng;
    }

    public void setHuoWuMingCheng(String huoWuMingCheng) {
        this.huoWuMingCheng = huoWuMingCheng;
    }
        
    public Integer getHuoWuShuLiang() {
        return huoWuShuLiang;
    }

    public void setHuoWuShuLiang(Integer huoWuShuLiang) {
        this.huoWuShuLiang = huoWuShuLiang;
    }
        
    public String getHuoWuZhongLiang() {
        return huoWuZhongLiang;
    }

    public void setHuoWuZhongLiang(String huoWuZhongLiang) {
        this.huoWuZhongLiang = huoWuZhongLiang;
    }
        
    public String getHuoWuTiJi() {
        return huoWuTiJi;
    }

    public void setHuoWuTiJi(String huoWuTiJi) {
        this.huoWuTiJi = huoWuTiJi;
    }
        
    public Integer getChengYunDanId() {
        return chengYunDanId;
    }

    public void setChengYunDanId(Integer chengYunDanId) {
        this.chengYunDanId = chengYunDanId;
    }
        
    public Integer getShuJuJiLuZhuangTai() {
        return shuJuJiLuZhuangTai;
    }

    public void setShuJuJiLuZhuangTai(Integer shuJuJiLuZhuangTai) {
        this.shuJuJiLuZhuangTai = shuJuJiLuZhuangTai;
    }


    @Override
    public String toString() {
        return "HuoWu{" +
                "id=" + id +
                ", huoWuMingCheng='" + huoWuMingCheng + '\'' +
                ", huoWuShuLiang=" + huoWuShuLiang +
                ", huoWuZhongLiang='" + huoWuZhongLiang + '\'' +
                ", huoWuTiJi='" + huoWuTiJi + '\'' +
                ", chengYunDanId=" + chengYunDanId +
                ", shuJuJiLuZhuangTai=" + shuJuJiLuZhuangTai +
                ", chengYunDan=" + chengYunDan +
                '}';
    }
}