package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 调度任务信息表(DiaoDuRenWu)实体类
 *
 * @author yangToT
 * @since 2021-03-19 16:55:50
 * @version 1.0
 */
@ApiModel(value = "DiaoDuRenWu",description = "调度任务信息表")
public class DiaoDuRenWu implements Serializable {
    private static final long serialVersionUID = 394511264080008934L;
    /**
    * 调度编号
    */
	@ApiModelProperty(name = "id",notes = "调度编号",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 出发时间
    */
	@ApiModelProperty(name = "chuFaShiJian",notes = "出发时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date chuFaShiJian;
    /**
    * 承运单Id
    */
	@ApiModelProperty(name = "chengYunDanId",notes = "承运单Id",dataType = "Integer",required = true)
    private Integer chengYunDanId;
    /**
    * 车辆Id
    */
	@ApiModelProperty(name = "cheLiangXinXiId",notes = "车辆Id",dataType = "Integer",required = true)
    private Integer cheLiangXinXiId;
    /**
    * 油费
    */
	@ApiModelProperty(name = "youFei",notes = "油费",dataType = "String",required = true)
    private String youFei;
    /**
    * 过桥费
    */
	@ApiModelProperty(name = "guoQiaoFei",notes = "过桥费",dataType = "String",required = true)
    private String guoQiaoFei;
    /**
    * 罚款
    */
	@ApiModelProperty(name = "faKuan",notes = "罚款",dataType = "String",required = true)
    private String faKuan;
    /**
    * 其他费用
    */
	@ApiModelProperty(name = "qiTaFeiYong",notes = "其他费用",dataType = "String",required = true)
    private String qiTaFeiYong;
    /**
    * 合计成本
    */
	@ApiModelProperty(name = "heJiChengBen",notes = "合计成本",dataType = "String",required = true)
    private String heJiChengBen;
    /**
    * 调度员  用户Id
    */
	@ApiModelProperty(name = "userId",notes = "调度员  用户Id",dataType = "Integer",required = true)
    private Integer userId;
    /**
    * 备注
    */
	@ApiModelProperty(name = "beiZhu",notes = "备注",dataType = "String",required = true)
    private String beiZhu;
    /**
    * 调度时间
    */
	@ApiModelProperty(name = "diaoDuShiJian",notes = "调度时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date diaoDuShiJian;
    /**
    * 数据记录状态 : 0:使用中	1:该记录已删
除
    */
	@ApiModelProperty(name = "shuJuJiLuZhuangTai",notes = "数据记录状态 : 0:使用中	1:该记录已删除",dataType = "Integer",required = true)
    private Integer shuJuJiLuZhuangTai;
    /**
    * 修改时间
    */
	@ApiModelProperty(name = "xiuGaiShiJian",notes = "修改时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xiuGaiShiJian;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public Date getChuFaShiJian() {
        return chuFaShiJian;
    }

    public void setChuFaShiJian(Date chuFaShiJian) {
        this.chuFaShiJian = chuFaShiJian;
    }
        
    public Integer getChengYunDanId() {
        return chengYunDanId;
    }

    public void setChengYunDanId(Integer chengYunDanId) {
        this.chengYunDanId = chengYunDanId;
    }
        
    public Integer getCheLiangXinXiId() {
        return cheLiangXinXiId;
    }

    public void setCheLiangXinXiId(Integer cheLiangXinXiId) {
        this.cheLiangXinXiId = cheLiangXinXiId;
    }
        
    public String getYouFei() {
        return youFei;
    }

    public void setYouFei(String youFei) {
        this.youFei = youFei;
    }
        
    public String getGuoQiaoFei() {
        return guoQiaoFei;
    }

    public void setGuoQiaoFei(String guoQiaoFei) {
        this.guoQiaoFei = guoQiaoFei;
    }
        
    public String getFaKuan() {
        return faKuan;
    }

    public void setFaKuan(String faKuan) {
        this.faKuan = faKuan;
    }
        
    public String getQiTaFeiYong() {
        return qiTaFeiYong;
    }

    public void setQiTaFeiYong(String qiTaFeiYong) {
        this.qiTaFeiYong = qiTaFeiYong;
    }
        
    public String getHeJiChengBen() {
        return heJiChengBen;
    }

    public void setHeJiChengBen(String heJiChengBen) {
        this.heJiChengBen = heJiChengBen;
    }
        
    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
        
    public String getBeiZhu() {
        return beiZhu;
    }

    public void setBeiZhu(String beiZhu) {
        this.beiZhu = beiZhu;
    }
        
    public Date getDiaoDuShiJian() {
        return diaoDuShiJian;
    }

    public void setDiaoDuShiJian(Date diaoDuShiJian) {
        this.diaoDuShiJian = diaoDuShiJian;
    }
        
    public Integer getShuJuJiLuZhuangTai() {
        return shuJuJiLuZhuangTai;
    }

    public void setShuJuJiLuZhuangTai(Integer shuJuJiLuZhuangTai) {
        this.shuJuJiLuZhuangTai = shuJuJiLuZhuangTai;
    }
        
    public Date getXiuGaiShiJian() {
        return xiuGaiShiJian;
    }

    public void setXiuGaiShiJian(Date xiuGaiShiJian) {
        this.xiuGaiShiJian = xiuGaiShiJian;
    }


    private ChengYunDan chengYunDan;

    private CheLiangXinXi cheLiangXinXi;

    private User user;

    public ChengYunDan getChengYunDan() {
        return chengYunDan;
    }

    public void setChengYunDan(ChengYunDan chengYunDan) {
        this.chengYunDan = chengYunDan;
    }

    public CheLiangXinXi getCheLiangXinXi() {
        return cheLiangXinXi;
    }

    public void setCheLiangXinXi(CheLiangXinXi cheLiangXinXi) {
        this.cheLiangXinXi = cheLiangXinXi;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "DiaoDuRenWu{" +
                "id=" + id +
                ", chuFaShiJian=" + chuFaShiJian +
                ", chengYunDanId=" + chengYunDanId +
                ", cheLiangXinXiId=" + cheLiangXinXiId +
                ", youFei='" + youFei + '\'' +
                ", guoQiaoFei='" + guoQiaoFei + '\'' +
                ", faKuan='" + faKuan + '\'' +
                ", qiTaFeiYong='" + qiTaFeiYong + '\'' +
                ", heJiChengBen='" + heJiChengBen + '\'' +
                ", userId=" + userId +
                ", beiZhu='" + beiZhu + '\'' +
                ", diaoDuShiJian=" + diaoDuShiJian +
                ", shuJuJiLuZhuangTai=" + shuJuJiLuZhuangTai +
                ", xiuGaiShiJian=" + xiuGaiShiJian +
                ", chengYunDan=" + chengYunDan +
                ", cheLiangXinXi=" + cheLiangXinXi +
                ", user=" + user +
                '}';
    }
}