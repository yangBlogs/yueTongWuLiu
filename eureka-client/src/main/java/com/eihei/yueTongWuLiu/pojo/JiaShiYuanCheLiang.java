package com.eihei.yueTongWuLiu.pojo;

import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 驾驶员车辆绑定表(JiaShiYuanCheLiang)实体类
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
@ApiModel(value = "JiaShiYuanCheLiang",description = "驾驶员车辆绑定表")
public class JiaShiYuanCheLiang implements Serializable {
    private static final long serialVersionUID = -46261800117708285L;
    /**
    * 联系编号
    */
	@ApiModelProperty(name = "id",notes = "联系编号",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 车辆信息Id
    */
	@ApiModelProperty(name = "cheLiangXinXiId",notes = "车辆信息Id",dataType = "Integer",required = true)
    private Integer cheLiangXinXiId;
    /**
    * 驾驶员信息Id
    */
	@ApiModelProperty(name = "jiaShiYuanXinXiId",notes = "驾驶员信息Id",dataType = "Integer",required = true)
    private Integer jiaShiYuanXinXiId;

	private CheLiangXinXi cheLiangXinXi;

	private JiaShiYuanXinXi jiaShiYuanXinXi;

    public CheLiangXinXi getCheLiangXinXi() {
        return cheLiangXinXi;
    }

    public void setCheLiangXinXi(CheLiangXinXi cheLiangXinXi) {
        this.cheLiangXinXi = cheLiangXinXi;
    }

    public JiaShiYuanXinXi getJiaShiYuanXinXi() {
        return jiaShiYuanXinXi;
    }

    public void setJiaShiYuanXinXi(JiaShiYuanXinXi jiaShiYuanXinXi) {
        this.jiaShiYuanXinXi = jiaShiYuanXinXi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public Integer getCheLiangXinXiId() {
        return cheLiangXinXiId;
    }

    public void setCheLiangXinXiId(Integer cheLiangXinXiId) {
        this.cheLiangXinXiId = cheLiangXinXiId;
    }
        
    public Integer getJiaShiYuanXinXiId() {
        return jiaShiYuanXinXiId;
    }

    public void setJiaShiYuanXinXiId(Integer jiaShiYuanXinXiId) {
        this.jiaShiYuanXinXiId = jiaShiYuanXinXiId;
    }

    @Override
    public String toString() {
        return "JiaShiYuanCheLiang{" +
                "id=" + id +
                ", cheLiangXinXiId=" + cheLiangXinXiId +
                ", jiaShiYuanXinXiId=" + jiaShiYuanXinXiId +
                ", cheLiangXinXi=" + cheLiangXinXi +
                ", jiaShiYuanXinXi=" + jiaShiYuanXinXi +
                '}';
    }
}