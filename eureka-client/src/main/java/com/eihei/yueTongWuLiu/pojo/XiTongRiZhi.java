package com.eihei.yueTongWuLiu.pojo;

import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 系统日志表(XiTongRiZhi)实体类
 *
 * @author yangToT
 * @since 2021-03-25 09:52:10
 * @version 1.0
 */
@ApiModel(value = "XiTongRiZhi",description = "系统日志表")
public class XiTongRiZhi implements Serializable {
    private static final long serialVersionUID = -85069573362133047L;
    /**
    * 日志ID
    */
	@ApiModelProperty(name = "id",notes = "日志ID",dataType = "Integer",required = true)
    private Integer id;
    /**
    * 操作行为
    */
	@ApiModelProperty(name = "caoZuoXingWei",notes = "操作行为",dataType = "String",required = true)
    private String caoZuoXingWei;
    /**
    * 用户id
    */
	@ApiModelProperty(name = "username",notes = "用户id",dataType = "String",required = true)
    private String username;
    /**
    * 参数
    */
	@ApiModelProperty(name = "canShu",notes = "参数",dataType = "String",required = true)
    private String canShu;
    /**
    * 储存或称名
    */
	@ApiModelProperty(name = "cuCunGuoChengMing",notes = "储存或称名",dataType = "String",required = true)
    private String cuCunGuoChengMing;
    /**
    * IP
    */
	@ApiModelProperty(name = "ip",notes = "IP",dataType = "String",required = true)
    private String ip;
    /**
    * 写入时间
    */
	@ApiModelProperty(name = "xieRuShiJian",notes = "写入时间",dataType = "Date",required = true)
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    // 下面是服务器响应返回的内容，如果没有格式化转换，那么返回的内容是长毫秒数，接收mysql数据库中的数据也需要设置东八区+8时
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date xieRuShiJian;
    /**
    * 异常信息
    */
	@ApiModelProperty(name = "yiChangXinXi",notes = "异常信息",dataType = "String",required = true)
    private String yiChangXinXi;
    /**
    * 异常信息状态 0：正常 1：异常
    */
	@ApiModelProperty(name = "yiChangXinXiZhuangTai",notes = "异常信息状态 0：正常 1：异常",dataType = "Integer",required = true)
    private Integer yiChangXinXiZhuangTai;

        
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
        
    public String getCaoZuoXingWei() {
        return caoZuoXingWei;
    }

    public void setCaoZuoXingWei(String caoZuoXingWei) {
        this.caoZuoXingWei = caoZuoXingWei;
    }
        
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
        
    public String getCanShu() {
        return canShu;
    }

    public void setCanShu(String canShu) {
        this.canShu = canShu;
    }
        
    public String getCuCunGuoChengMing() {
        return cuCunGuoChengMing;
    }

    public void setCuCunGuoChengMing(String cuCunGuoChengMing) {
        this.cuCunGuoChengMing = cuCunGuoChengMing;
    }
        
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
        
    public Date getXieRuShiJian() {
        return xieRuShiJian;
    }

    public void setXieRuShiJian(Date xieRuShiJian) {
        this.xieRuShiJian = xieRuShiJian;
    }
        
    public String getYiChangXinXi() {
        return yiChangXinXi;
    }

    public void setYiChangXinXi(String yiChangXinXi) {
        this.yiChangXinXi = yiChangXinXi;
    }
        
    public Integer getYiChangXinXiZhuangTai() {
        return yiChangXinXiZhuangTai;
    }

    public void setYiChangXinXiZhuangTai(Integer yiChangXinXiZhuangTai) {
        this.yiChangXinXiZhuangTai = yiChangXinXiZhuangTai;
    }

}