package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.User;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 用户(User)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
public interface UserMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<User> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String mingCheng);

//    /**
//     * 通过实体作为筛选条件查询
//     *
//     * @param xiaBiao    当前查询开始页中的第一个下标值
//     * @param mingCheng  需要模糊查询的内容
//     * @return 对象列表
//     */
//    List<User> chaXunFenYe(@Param("xiaBiao") int xiaBiao,
//                           @Param("mingCheng")String mingCheng,
//                           String username,String zhangHao,String phone,Integer jueSeId);

    /**
     * 新增数据
     *
     * @param user 实例对象
     */
    void xinZeng(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 影响行数
     */
    int gengXinById(User user);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") Integer id);


    /**
     * 通过用户手机号查询用户信息Id
     *
     * @param phone 主键
     * @return 实例对象
     */
    int chaXunId(@Param("phone") String phone);

}