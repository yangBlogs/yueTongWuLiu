package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.JiaShiYuanXinXi;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 驾驶员信息表(JiaShiYuanXinXi)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
public interface JiaShiYuanXinXiMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<JiaShiYuanXinXi> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    JiaShiYuanXinXi chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String mingCheng);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param xiaBiao    当前查询开始页中的第一个下标值
     * @param mingCheng  需要模糊查询的内容
     * @return 对象列表
     */
    List<JiaShiYuanXinXi> chaXunFenYe(@Param("xiaBiao") int xiaBiao, @Param("mingCheng")String mingCheng);

    /**
     * 新增数据
     *
     * @param jiaShiYuanXinXi 实例对象
     */
    void xinZeng(JiaShiYuanXinXi jiaShiYuanXinXi);

    /**
     * 修改数据
     *
     * @param jiaShiYuanXinXi 实例对象
     * @return 影响行数
     */
    int gengXinById(JiaShiYuanXinXi jiaShiYuanXinXi);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") Integer id);

    //根据驾驶员身份证号吗查询驾驶员Id
    int chaXunId(String shengFenZhengHaoMa);

}