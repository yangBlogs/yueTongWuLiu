package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.DiaoDuRenWu;
import com.eihei.yueTongWuLiu.pojo.HuoWu;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 调度任务信息表(DiaoDuRenWu)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
public interface DiaoDuRenWuMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<DiaoDuRenWu> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    DiaoDuRenWu chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String mingCheng);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param xiaBiao    当前查询开始页中的第一个下标值
     * @param mingCheng  需要模糊查询的内容
     * @return 对象列表
     */
    List<DiaoDuRenWu> chaXunFenYe(@Param("xiaBiao") int xiaBiao, @Param("mingCheng")String mingCheng);

    /**
     * 新增数据
     *
     * @param diaoDuRenWu 实例对象
     */
    void xinZeng(DiaoDuRenWu diaoDuRenWu);

    /**
     * 修改数据
     *
     * @param diaoDuRenWu 实例对象
     * @return 影响行数
     */
    int gengXinById(DiaoDuRenWu diaoDuRenWu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") Integer id);

    /**
     * 通过查询调度任务ID
     *
     * @return 实例对象
     */
    DiaoDuRenWu chaXunId(@Param("chengYunDanId") Integer chengYunDanId,
                   @Param("cheLiangXinXiId") Integer cheLiangXinXiId,
                   @Param("diaoDuShiJian") String diaoDuShiJian);

}