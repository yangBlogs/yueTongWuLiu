package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.CheDuiXinXi;
import com.eihei.yueTongWuLiu.pojo.CheLiangXinXi;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 车辆信息表(CheLiangXinXi)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-17 10:14:08
 * @version 1.0
 */
public interface CheLiangXinXiMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<CheLiangXinXi> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CheLiangXinXi chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String mingCheng);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param xiaBiao    当前查询开始页中的第一个下标值
     * @param mingCheng  需要模糊查询的内容
     * @return 对象列表
     */
    List<CheLiangXinXi> chaXunFenYe(@Param("xiaBiao") int xiaBiao, @Param("mingCheng")String mingCheng);

    /**
     * 新增数据
     *
     * @param cheLiangXinXi 实例对象
     */
    void xinZeng(CheLiangXinXi cheLiangXinXi);

    /**
     * 修改数据
     *
     * @param cheLiangXinXi 实例对象
     * @return 影响行数
     */
    int gengXinById(CheLiangXinXi cheLiangXinXi);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") Integer id);

    //通过名称查询车辆Id
    int chaXunId(String chePaiHaoMa);



}