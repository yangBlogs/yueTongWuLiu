package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.HuoWu;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 货物表(HuoWu)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-18 21:17:09
 * @version 1.0
 */
public interface HuoWuMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<HuoWu> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    HuoWu chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String mingCheng);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param xiaBiao    当前查询开始页中的第一个下标值
     * @param mingCheng  需要模糊查询的内容
     * @return 对象列表
     */
    List<HuoWu> chaXunFenYe(@Param("xiaBiao") int xiaBiao, @Param("mingCheng")String mingCheng);

    /**
     * 新增数据
     *
     * @param huoWu 实例对象
     */
    void xinZeng(HuoWu huoWu);

    /**
     * 修改数据
     *
     * @param huoWu 实例对象
     * @return 影响行数
     */
    int gengXinById(HuoWu huoWu);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") String id);

    /**
     * 通过货物名称,承运单Id查询货物ID
     *
     * @return 实例对象
     */
    HuoWu chaXunId(@Param("huoWuMingCheng") String huoWuMingCheng,
                   @Param("chengYunDanId") Integer chengYunDanId);

}