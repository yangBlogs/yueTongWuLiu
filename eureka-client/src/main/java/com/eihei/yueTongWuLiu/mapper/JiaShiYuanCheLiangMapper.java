package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.DiaoDuRenWu;
import com.eihei.yueTongWuLiu.pojo.JiaShiYuanCheLiang;
import org.apache.ibatis.annotations.Param;

/**
 * 驾驶员车辆绑定表(JiaShiYuanCheLiang)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-15 21:00:23
 * @version 1.0
 */
public interface JiaShiYuanCheLiangMapper {

//    /**
//     * 为驾驶员绑定车辆信息
//     *
//     * @param jiaShiYuanCheLiang 实例对象
//     */
//    void bangDingCheLiang(JiaShiYuanCheLiang jiaShiYuanCheLiang);

    /**
     * 修改数据
     *
     * @return 影响行数
     */
    int bangDingCheLiang (JiaShiYuanCheLiang jiaShiYuanCheLiang);

    /**
     * 通过驾驶员Id解绑车辆信息
     *
     * @param id 主键
     * @return 影响行数
     */
    int jieBangCheLiang(@Param("id") Integer id);

    //通过驾驶员Id以及车辆Id 查询主键Id
//    int chaXunID(JiaShiYuanCheLiang jiaShiYuanCheLiang);
//    int chaXunID(@Param("jiaShiYuanXinXiId") Integer jiaShiYuanXinXiId);

    /**
     * 新增数据
     *
     * @param jiaShiYuanXinXiId 驾驶员信息ID
     */
    void xinZeng(@Param("jiaShiYuanXinXiId") Integer jiaShiYuanXinXiId);

    JiaShiYuanCheLiang chaXunId(@Param("jiaShiYuanXinXiId") Integer jiaShiYuanXinXiId);

    JiaShiYuanCheLiang chaXunById(@Param("id") Integer id);


}