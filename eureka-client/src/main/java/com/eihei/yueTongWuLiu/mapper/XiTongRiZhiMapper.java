package com.eihei.yueTongWuLiu.mapper;

import com.eihei.yueTongWuLiu.pojo.XiTongRiZhi;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 系统日志表(XiTongRiZhi)表数据库访问层
 *
 * @author yangToT
 * @since 2021-03-25 09:52:10
 * @version 1.0
 */
public interface XiTongRiZhiMapper {

    /**
     * 查询所有数据
     * @return  返回所有数据
     */
    List<XiTongRiZhi> chaXunAll();

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    XiTongRiZhi chaXunById(@Param("id") Integer id);

    /**
     * 根据模糊条件查询总个数
     *
     * @return 返回查询到的总个数
     */
    int chaXunCount(String caoZuoXingWei,String username,String xieRuShiJian_KaiSHi,String xieRuShiJian_JieShu);

    /**
     * 通过实体作为筛选条件查询
     * 
     * @param xiaBiao    当前查询开始页中的第一个下标值
     * @return 对象列表
     */
    List<XiTongRiZhi> chaXunFenYe(@Param("xiaBiao") int xiaBiao,
                                  @Param("caoZuoXingWei")String caoZuoXingWei,
                                  @Param("username")String username,
                                  @Param("xieRuShiJian_KaiSHi")String xieRuShiJian_KaiSHi,
                                  @Param("xieRuShiJian_JieShu")String xieRuShiJian_JieShu);

    /**
     * 新增数据
     *
     * @param xiTongRiZhi 实例对象
     */
    void xinZeng(XiTongRiZhi xiTongRiZhi);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int shanChuById(@Param("id") String id);

}